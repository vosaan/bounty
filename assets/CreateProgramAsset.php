<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class CreateProgramAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        parent::init();
        $this->js = [
            'js/owner/jquery.validate.min.js?v=' . time(),
            'js/owner/additional-methods.min.js?v=' . time(),
            'js/owner/modules.js?v=' . time(),
            'js/bootstrap2-toggle.min.js?v=' . time(),
            'js/owner/bootstrap-slider.min.js?v=' . time(),
        ];

        $this->css = [
            'css/site.css?v=' . time(),
            'css/style.css?v=' . time(),
            'css/bootstrap2-toggle.min.css?v=' . time(),
            'css/owner/bootstrap-slider.min.css?v=' . time(),
            'css/owner/create-program.css?v=' . time(),
        ];
    }
}