<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class HunterAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        parent::init();
        $this->js = [
            'js/same-tab.js?v=' . time(),
            'js/hunter/your-account.js?v=' . time(),
        ];

        $this->css = [
            'css/site.css?v=' . time(),
            'css/style.css?v=' . time(),
            'css/hunter/hunter.css?v=' . time(),
        ];
    }
}