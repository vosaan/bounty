<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bounty_program".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $status_id
 * @property int $owner_id
 * @property string $term_start
 * @property string $term_stop
 * @property int $program_type_id
 * @property double $common_budget
 * @property double $token_coefficient
 * @property string $official_site_link
 * @property string $bitcoin_talk_thread_link
 * @property string $bitcoin_talk_bounty_program_link
 * @property int $coefficient_currency_id
 * @property string $rules
 *
 * @property DirProgramType $programType
 * @property User $owner
 * @property DirProgramStatus $status
 * @property BountyProgramAction[] $bountyProgramActions
 * @property BountyProgramModule[] $bountyProgramModules
 */
class BountyProgram extends \yii\db\ActiveRecord
{
    public $isNewRecord = true;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bounty_program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'status_id', 'owner_id', 'term_start', 'term_stop', 'program_type_id', 'common_budget', 'token_coefficient', 'official_site_link', 'bitcoin_talk_thread_link', 'bitcoin_talk_bounty_program_link', 'coefficient_currency_id'], 'required'],
            [['description'], 'string'],
            [['status_id', 'owner_id', 'program_type_id', 'coefficient_currency_id'], 'integer'],
            [['term_start', 'term_stop', 'rules'], 'safe'],
            [['common_budget', 'token_coefficient'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['official_site_link', 'bitcoin_talk_thread_link', 'bitcoin_talk_bounty_program_link'],'url', 'defaultScheme' => 'http'],
            [['program_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirProgramType::className(), 'targetAttribute' => ['program_type_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirProgramStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'status_id' => 'Status ID',
            'owner_id' => 'Owner ID',
            'term_start' => 'Term Start',
            'term_stop' => 'Term Stop',
            'program_type_id' => 'Program Type ID',
            'common_budget' => 'Common Budget',
            'token_coefficient' => 'Token Coefficient',
            'official_site_link' => 'Official Site Link',
            'bitcoin_talk_thread_link' => 'Bitcoin Talk Thread Link',
            'bitcoin_talk_bounty_program_link' => 'Bitcoin Talk Bounty Program Link',
            'rules' => 'Program rules'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramType()
    {
        return $this->hasOne(DirProgramType::className(), ['id' => 'program_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(DirProgramStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBountyProgramActions()
    {
        return $this->hasMany(BountyProgramAction::className(), ['program_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBountyProgramModules()
    {
        return $this->hasMany(BountyProgramModule::className(), ['program_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoefficientCurrency()
    {
        return $this->hasOne(DirCoefficientCurrency::className(), ['id' => 'coefficient_currency_id']);
    }
}
