<?php
namespace app\models;

use yii\base\Model;
use app\models\DirModule;

/**
 *
 * @property DirModule $id
 * @property int $program_id
 * @property int $module_id
 * @property string $module_target_link
 * @property double $module_budget
 * @property int $current_module_status
 *
 * @property DirModule $module
 * @property BountyProgram $program
 */

class Module extends Model
{
    public $id;
    public $budget;
    public $targetLink;
    public $actions = [];

}