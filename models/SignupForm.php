<?php
namespace app\models;

use app\helpers\Utils;
use yii\base\Model;
use app\helpers\Mail;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $role;
    public $reCaptcha;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            //['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 6, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            ['email', 'unique', 'targetClass' => '\app\models\SocAccount', 'targetAttribute' => 'soc_user_email', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['role', 'safe'],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LcdL0UUAAAAAKpJ4GYt0zsgp_4EYFZ2bZPvP92j', 'uncheckedMessage' => 'Please confirm that you are not a bot.']
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }



        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->activation_code = Yii::$app->security->generateRandomString();
        $user->save();

        /* Assigning role for new user */
        Utils::assignRole($this->role, $user->id);

        Mail::sendActivationMail($user->email, $user->activation_code);

        return $user ? $user : null;
    }

    public function socEmailValidate($attribute, $params)
    {
        $socEmails = SocAccount::find()->select(['soc_user_email'])->column();
        if (in_array($attribute, $socEmails)) {
            $this->addError($attribute, 'пароль слишком слабый');
        }
    }
}
