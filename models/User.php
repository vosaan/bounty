<?php
namespace app\models;

use app\helpers\Utils;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $activation_code
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'default', 'value' => self::STATUS_NOT_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_NOT_ACTIVE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::find()->where(['username' => $username, 'status' => self::STATUS_ACTIVE])->with('role')->one();
        //return static::findOne(['username' => $username]);
    }

    public static function findByEmail($email)
    {
        return static::find()->where(['email' => $email, 'status' => self::STATUS_ACTIVE])->with('role')->one();
    }

    /**
     * Finds user by any e-mail (basic, facebook, twitter or google+)
     *
     * @param string $email
     * @return object|array
     */
    public static function findByAnyEmail($email)
    {
        return static::find()
            ->where(['email' => $email, 'status' => self::STATUS_ACTIVE])
            ->orWhere(['twitter_user_email' => $email, 'status' => self::STATUS_ACTIVE])
            ->orWhere(['facebook_user_email' => $email, 'status' => self::STATUS_ACTIVE])
            ->orWhere(['google_email' => $email, 'status' => self::STATUS_ACTIVE])
            ->with('role')->one();
    }

    /**
 * Finds Twitter user by twitter_user_id
 *
 * @param string $twitterUserId
 * @return static|null
 */
    public static function findTwitterUserByTwitterUserId($twitterUserId)
    {
        return static::findOne(['twitter_user_id' => $twitterUserId, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds Facebook user by facebook user id
     *
     * @param string $facebookUserId
     * @return static|null
     */
    public static function findFacebookUserByFacebookUserId($facebookUserId)
    {
        return static::findOne(['facebook_user_id' => $facebookUserId, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds Facebook user by facebook user id
     *
     * @param string $googleUserId
     * @return static|null
     */
    public static function findGoogleUserByGoogleUserId($googleUserId)
    {
        return static::findOne(['google_user_id' => $googleUserId, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function userLogin($user)
    {
        return Yii::$app->user->login($user, 3600 * 24 * 30);
    }

    public function getRole()
    {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'id']);
    }

    public function getOwnerPrograms(){
        return $this->hasMany(BountyProgram::className(), ['owner_id' => 'id']);
    }
}
