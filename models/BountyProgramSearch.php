<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BountyProgram;
use yii\helpers\ArrayHelper;

/**
 * BountyProgramSearch represents the model behind the search form of `app\models\BountyProgramHlp`.
 */
class BountyProgramSearch extends BountyProgram
{
    public $module_id;
	
	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id', 'owner_id', 'program_type_id', /*'currency_id',*/ 'coefficient_currency_id', 'module_id'], 'integer'],
            [['name', 'description', 'term_start', 'term_stop', 'official_site_link', 'bitcoin_talk_thread_link', 'bitcoin_talk_bounty_program_link'], 'safe'],
            [['common_budget', 'token_coefficient'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $statusActive = ArrayHelper::getValue(
            DirProgramStatus::find()->where(['name' => 'Active'])->one(),
            'id'
        );

        $query = BountyProgram::find()->where(['status_id' => $statusActive])->joinWith(['bountyProgramModules']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status_id' => $this->status_id,
            'owner_id' => $this->owner_id,
            'term_start' => $this->term_start,
            'term_stop' => $this->term_stop,
            'program_type_id' => $this->program_type_id,
            'common_budget' => $this->common_budget,
            'token_coefficient' => $this->token_coefficient,
            //'currency_id' => $this->currency_id,
            'coefficient_currency_id' => $this->coefficient_currency_id,
			BountyProgramModule::tableName() . '.module_id' => $this->module_id
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'official_site_link', $this->official_site_link])
            ->andFilterWhere(['like', 'bitcoin_talk_thread_link', $this->bitcoin_talk_thread_link])
            ->andFilterWhere(['like', 'bitcoin_talk_bounty_program_link', $this->bitcoin_talk_bounty_program_link]);

        return $dataProvider;
    }
}
