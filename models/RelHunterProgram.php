<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rel_hunter_program".
 *
 * @property int $id
 * @property int $hunter_primary_user_id
 * @property int $program_id
 *
 * @property BountyProgram $program
 * @property User $hunterPrimaryUser
 */
class RelHunterProgram extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_hunter_program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hunter_primary_user_id', 'program_id'], 'required'],
            [['hunter_primary_user_id', 'program_id'], 'integer'],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => BountyProgram::className(), 'targetAttribute' => ['program_id' => 'id']],
            [['hunter_primary_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['hunter_primary_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hunter_primary_user_id' => 'Hunter Primary User ID',
            'program_id' => 'Program ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(BountyProgram::className(), ['id' => 'program_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHunterPrimaryUser()
    {
        return $this->hasOne(User::className(), ['id' => 'hunter_primary_user_id']);
    }
}
