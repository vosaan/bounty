<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dir_program_type".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $status
 */
class DirProgramType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dir_program_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description', 'status'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'status' => 'Status',
        ];
    }

    public function getProgramModules()
    {
        return $this->hasMany(RelProgramTypeModule::className(), ['program_type_id' => 'id']);
    }
}
