<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bounty_program_module".
 *
 * @property int $id
 * @property int $program_id
 * @property int $module_id
 * @property string $module_target_link
 * @property double $module_budget
 * @property int $current_module_status
 * @property mixed $actions
 *
 * @property DirModule $module
 * @property BountyProgram $program
 */
class BountyProgramModule extends \yii\db\ActiveRecord
{
    const STATUS_PAUSED = 0;
    const STATUS_ACTIVE = 1;

    public function __construct($module_id = null, array $config = [])
    {
        $this->module_id = $module_id;
        $this->current_module_status = 1;
        parent::__construct($config);

    }

    public $actions;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bounty_program_module';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program_id', 'module_id', 'module_target_link', 'module_budget'], 'required'],
            [['program_id', 'module_id'], 'integer'],
            [['module_budget'], 'number'],
            [['module_target_link'], 'url', 'defaultScheme' => 'http'],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirModule::className(), 'targetAttribute' => ['module_id' => 'id']],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => BountyProgram::className(), 'targetAttribute' => ['program_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'program_id' => 'Program ID',
            'module_id' => 'Module ID',
            'module_target_link' => 'Module Target Link',
            'module_budget' => 'Module Budget',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(DirModule::className(), ['id' => 'module_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(BountyProgram::className(), ['id' => 'program_id']);
    }

    public function getModuleActions(){
        return $this->hasMany(BountyProgramAction::className(), ['module_id' => 'id']);
    }
}
