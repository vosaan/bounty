<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rel_module_action".
 *
 * @property int $module_id
 * @property int $action_id
 *
 * @property DirAction $action
 * @property DirModule $module
 */
class RelModuleAction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_module_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_id', 'action_id'], 'required'],
            [['module_id', 'action_id'], 'integer'],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirAction::className(), 'targetAttribute' => ['action_id' => 'id']],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirModule::className(), 'targetAttribute' => ['module_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'module_id' => 'Module ID',
            'action_id' => 'Action ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(DirAction::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(DirModule::className(), ['id' => 'module_id']);
    }
}
