<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bounty_program_action_reward".
 *
 * @property int $id
 * @property int $program_id
 * @property int $module_id
 * @property int $action_id
 * @property int $reward_level_id
 * @property int $followers_count
 * @property int $reward_value
 *
 * @property DirRewardLevel $rewardLevel
 * @property BountyProgram $program
 * @property BountyProgramAction $action
 * @property BountyProgramModule $module
 */
class BountyProgramActionReward extends \yii\db\ActiveRecord
{
    public function __construct($reward_level_id = null, array $config = [])
    {
        $this->reward_level_id = $reward_level_id;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bounty_program_action_reward';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program_id', 'module_id', 'action_id', 'reward_level_id', 'followers_count', 'reward_value'], 'required'],
            [['program_id', 'module_id', 'action_id', 'reward_level_id', 'followers_count', 'reward_value'], 'integer'],
            [['reward_level_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirRewardLevel::className(), 'targetAttribute' => ['reward_level_id' => 'id']],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => BountyProgram::className(), 'targetAttribute' => ['program_id' => 'id']],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => BountyProgramAction::className(), 'targetAttribute' => ['action_id' => 'id']],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => BountyProgramModule::className(), 'targetAttribute' => ['module_id' => 'id']],
            [['followers_count', 'reward_value'], 'number', 'min' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'program_id' => 'Program ID',
            'module_id' => 'Module ID',
            'action_id' => 'Action ID',
            'reward_level_id' => 'Reward Level ID',
            'followers_count' => 'Followers Count',
            'reward_value' => 'Reward Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRewardLevel()
    {
        return $this->hasOne(DirRewardLevel::className(), ['id' => 'reward_level_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(BountyProgram::className(), ['id' => 'program_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(BountyProgramAction::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(BountyProgramModule::className(), ['id' => 'module_id']);
    }
}
