<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dir_module".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $status
 * @property int $module_type_id
 *
 * @property DirModuleType $moduleType
 */
class DirModule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dir_module';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'module_type_id'], 'required'],
            [['description', 'status'], 'string'],
            [['module_type_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['module_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirModuleType::className(), 'targetAttribute' => ['module_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'status' => 'Status',
            'module_type_id' => 'Module Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModuleType()
    {
        return $this->hasOne(DirModuleType::className(), ['id' => 'module_type_id']);
    }

    public function getActions()
    {
        return $this->hasMany(RelModuleAction::className(), ['module_id' => 'id']);
    }
}
