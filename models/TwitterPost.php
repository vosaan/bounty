<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "twitter_post".
 *
 * @property int $id
 * @property int $owner_id
 * @property int $program_id
 * @property int $program_module_id
 * @property string $post_id
 * @property int $post_create_date
 * @property int $post_add_date
 *
 * @property BountyProgram $program
 * @property BountyProgramModule $programModule
 * @property User $owner
 */
class TwitterPost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'twitter_post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['owner_id', 'program_id', 'program_module_id', 'post_create_date', 'post_add_date'], 'required'],
            [['owner_id', 'program_id', 'program_module_id', 'post_id', 'post_create_date', 'post_add_date'], 'integer'],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => BountyProgram::className(), 'targetAttribute' => ['program_id' => 'id']],
            [['program_module_id'], 'exist', 'skipOnError' => true, 'targetClass' => BountyProgramModule::className(), 'targetAttribute' => ['program_module_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'program_id' => 'Program ID',
            'program_module_id' => 'Program Module ID',
            'post_id' => 'Post ID',
            'post_create_date' => 'Post Create Date',
            'post_add_date' => 'Post Add Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(BountyProgram::className(), ['id' => 'program_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramModule()
    {
        return $this->hasOne(BountyProgramModule::className(), ['id' => 'program_module_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
}
