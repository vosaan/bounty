<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "soc_account".
 *
 * @property int $id
 * @property int $primary_user_id
 * @property string $soc_network_name
 * @property string $soc_user_id
 * @property string $soc_user_name
 * @property string $soc_user_email
 * @property string $soc_user_pic
 * @property int $created_at
 * @property int $soc_user_followers_count
 * @property int $soc_user_screen_name
 * @property int $linking_request_code
 */
class SocAccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'soc_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['primary_user_id', 'created_at'], 'integer'],
            [['created_at'], 'required'],
            [['soc_network_name', 'soc_user_screen_name', 'soc_user_id', 'soc_user_name', 'soc_user_email', 'soc_user_pic'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'primary_user_id' => 'Primary User ID',
            'soc_network_name' => 'Soc Network Name',
            'soc_user_id' => 'Soc User ID',
            'soc_user_name' => 'Soc User Name',
            'soc_user_email' => 'Soc User Email',
            'soc_user_pic' => 'Soc User Pic',
            'created_at' => 'Created At',
        ];
    }
}
