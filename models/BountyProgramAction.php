<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bounty_program_action".
 *
 * @property int $id
 * @property int $program_id
 * @property int $module_id
 * @property int $action_id
 * @property string $deadline
 * @property mixed $rewards
 *
 * @property DirAction $action
 * @property BountyProgramModule $module
 * @property BountyProgram $program
 */
class BountyProgramAction extends \yii\db\ActiveRecord
{
    public function __construct($action_id = null, array $config = [])
    {
        $this->action_id = $action_id;
        parent::__construct($config);

    }

    public $rewards;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bounty_program_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program_id', 'module_id', 'action_id'], 'required'],
            [['program_id', 'module_id', 'action_id'], 'integer'],
            [['deadline'], 'string'],
            [['deadline'], 'safe'],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirAction::className(), 'targetAttribute' => ['action_id' => 'id']],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => BountyProgramModule::className(), 'targetAttribute' => ['module_id' => 'id']],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => BountyProgram::className(), 'targetAttribute' => ['program_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'program_id' => 'Program ID',
            'module_id' => 'Module ID',
            'action_id' => 'Action ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(DirAction::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(BountyProgramModule::className(), ['id' => 'module_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(BountyProgram::className(), ['id' => 'program_id']);
    }
}
