<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rel_program_type_module".
 *
 * @property int $id
 * @property int $program_type_id
 * @property int $module_id
 *
 * @property DirProgramType $programType
 * @property DirModule $module
 */
class RelProgramTypeModule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_program_type_module';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program_type_id', 'module_id'], 'required'],
            [['program_type_id', 'module_id'], 'integer'],
            [['program_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirProgramType::className(), 'targetAttribute' => ['program_type_id' => 'id']],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirModule::className(), 'targetAttribute' => ['module_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'program_type_id' => 'Program Type ID',
            'module_id' => 'Module ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramType()
    {
        return $this->hasOne(DirProgramType::className(), ['id' => 'program_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(DirModule::className(), ['id' => 'module_id']);
    }
}
