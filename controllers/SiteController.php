<?php

namespace app\controllers;

use Abraham\TwitterOAuth\Util;
use app\helpers\FacebookAuth;
use app\helpers\Mail;
use app\helpers\TwitterAuth;
use app\helpers\GoogleAuth;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\SignupForm;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SocAccount;
use yii\helpers\Html;
use app\models\User;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use app\helpers\Utils;
use app\helpers\SocUser;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity) {
            return $this->redirectByUserRole(Yii::$app->user->identity->role['item_name']);
        }

        return $this->render('index');
    }

    /**
     * User activation by email
     *
     * @return string
     */
    public function actionActivation(){
        $code = Yii::$app->request->get('code');
        $code = Html::encode($code);

        $user = User::find()->where(['activation_code'=>$code])->one();
        if ($user){
            if ($user->status == User::STATUS_ACTIVE) {
                return $this->render('already-activated');
            }

            $user->status = User::STATUS_ACTIVE;
            if ($user->save()) {

                $role = $user->role->item_name;
                if ($role == 'hunter') {
                    $session = Yii::$app->session;
                    $email = $session->get('email');
                    $password = $session->get('password');
                    Mail::sendPasswordMail($email, $password);
                    $session->remove('email');
                    $session->remove('password');
                }
                return $this->render('confirmed-email');
            }
        }
        return $this->render('code-does-not-match');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirectByUserRole($model->user->role['item_name']);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * User signup with Twitter account
     *
     * @return string
     */
    public function actionAuthWithTwitter()
    {
        if (Yii::$app->request->get('oauth_verifier')) {
            $userData = TwitterAuth::getUserData(Yii::$app->request->get('oauth_verifier'));

            if ($userData) {
                $this->loginOrCreateUser($userData);
            }
        }
    }

    public function actionAuthWithFacebook()
    {
        $userData = FacebookAuth::getUserData();
        if ($userData->warning) {
            return $this->actionFacebookReauth($userData);
        } else {
            $this->loginOrCreateUser($userData);
        }
    }

    public function actionAuthWithGoogle()
    {
        if (Yii::$app->request->get('code')) {
            $code = Yii::$app->request->get('code');
            $userData = GoogleAuth::getUserData($code);

            if ($userData) {
                $this->loginOrCreateUser($userData);
            }
        }
    }

    private function loginOrCreateUser($userData){
        /* Получаем объект соц.аккаунта */
        $existingSocUser = SocAccount::find()->where(['soc_user_id' => $userData->soc_user_id])->one();

        /* Если такого не существует */
        if (!$existingSocUser) {

            /* Ищем primary-аккаунт с таким же email, как и у соц.аккаунта */
            $primaryUser = User::find()->where(['email' => $userData->soc_user_email])->one();

            /* Если такой primary-аккаунт существует */
            if ($primaryUser) {

                /* добавляем к данным соц.пользователя принадлежность к этому primary-аккаунту */
                $userData->primary_user_id = $primaryUser->id;

                /* создаём соц.пользователя */
                SocUser::create($userData);

                /* логиним, но с данными primary-аккаунта */
                User::userLogin($primaryUser);
                return $this->redirectByUserRole($primaryUser->role['item_name']);
            } else {
                /* если primary-акаунта не существует, то создаём и его, и соц.аккаунт */
                $userPassword = Yii::$app->security->generateRandomString(8);

                $user = new User();
                $user->status = User::STATUS_NOT_ACTIVE;
                $user->activation_code = Yii::$app->security->generateRandomString();
                $user->password_reset_token = NULL;
                $user->auth_key = Yii::$app->security->generateRandomString();
                $user->password_hash = Yii::$app->security->generatePasswordHash($userPassword);
                $user->email = $userData->soc_user_email;
                $user->save();

                Utils::assignRole('hunter', $user->id);

                $userData->primary_user_id = $user->id;
                SocUser::create($userData);

                Mail::sendActivationMail($user->email, $user->activation_code);

                $session = Yii::$app->session;
                $session->set('email', $user->email);
                $session->set('password', $userPassword);
                return $this->redirect('/confirm-email');
            }
        } else {
            /* либо логиним его с данными primary */
            $user = User::findIdentity($existingSocUser->primary_user_id);

            /* Если не прошел активацию */
            if (!$user) {
                $session = Yii::$app->session;
                $session->set('email', $existingSocUser->soc_user_email);
                return $this->redirect('/confirm-email');
            }

            User::userLogin($user);
            return $this->redirectByUserRole($user->role['item_name']);
        }
    }

    public function actionPleaseRegister()
    {
        return $this->render('please-register');
    }

    public function actionFacebookReauth($userData)
    {
        return $this->render('facebook-reauth', compact('userData'));
    }

    public function actionSignUpAsHunter()
    {
        $model = new SignupForm();
        $model->role = 'hunter';
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                return $this->render('confirm-email');
            }
        }

        return $this->render('signup', [
            'model' => $model,
            'role' => 'hunter'
        ]);
    }

    public function actionSignUpAsOwner()
    {
        $model = new SignupForm();
        $model->role = 'owner';
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                return $this->render('confirm-email');
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionConfirmEmail()
    {
        $session = Yii::$app->session;
        $email = $session->get('email');
        return $this->render('confirm-email', ['email' => $email]);
    }

    private function redirectByUserRole($role){
        switch ($role){
            case 'admin':
                return $this->redirect('/admin');
            case 'hunter':
                return $this->redirect('/hunter');
            case 'owner':
                return $this->redirect('/owner');
            case 'manager':
                return $this->redirect('/owner');
            case 'support':
                return $this->redirect('/owner');
            case 'analist':
                return $this->redirect('/owner');
        }
    }
}
