<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'v6jxkTK0gXFLUyw8Gr1RP4mJlde_S_wW',
            'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                //'<action:\w+>' => 'site/<action>',
                'activation/<code:.+>' => 'site/activation',
                'hunter/your-account/linking-request/<secret:.+>' => 'hunter/your-account/linking-request',
                'hunter/your-account/email-changing/<secret:.+>' => 'hunter/your-account/email-changing',
                'reset-password/<token:.+>' => 'site/reset-password',
                'sign-up-as-hunter' => 'site/sign-up-as-hunter',
                'sign-up-as-owner' => 'site/sign-up-as-owner',
                'confirm-email' => 'site/confirm-email',
                'login' => 'site/login',
                'site/signup' => ''
            ],
        ],

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache'
        ],

        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LcdL0UUAAAAAPTjrOzo_rXNds2tz4E0KKBWf1Bt',
            'secret' => '6LcdL0UUAAAAAKpJ4GYt0zsgp_4EYFZ2bZPvP92j',
        ],

        'encrypter' => [
            'class'=>'\nickcv\encrypter\components\Encrypter',
            'globalPassword'=>'rXNds2tz4E0KKBWf1Bt',
            'iv'=>'YWJjZGVmZ2hpZ2t2',
            'useBase64Encoding'=>true,
            'use256BitesEncoding'=>false,
        ],

    ],

    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                    [
                        'allow' => false,
                        'roles' => ['?', 'hunter', 'owner', 'manager', 'support', 'analist'],
                    ],
                ],
            ],
        ],

        'hunter' => [
            'class' => 'app\modules\hunter\Module',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['hunter'],
                    ],

                    [
                        'allow' => false,
                        'roles' => ['?', 'admin', 'owner', 'manager', 'support', 'analist'],
                    ],
                ],
            ],
            'defaultRoute' => 'dashboard/index'
        ],

        'owner' => [
            'class' => 'app\modules\owner\Module',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['owner', 'manager', 'support', 'analist'],
                    ],

                    [
                        'allow' => false,
                        'roles' => ['?', 'admin', 'hunter'],
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
