<?php

namespace app\modules\hunter;

/**
 * hunter module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\hunter\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->layout = 'hunter';
        // custom initialization code goes here
    }
}
