<?php

namespace app\modules\hunter\controllers;

use app\helpers\FacebookAuth;
use app\helpers\Mail;
use app\helpers\TwitterAuth;
use app\helpers\GoogleAuth;
use app\helpers\Utils;
use app\models\SocAccount;
use app\helpers\SocUser;
use app\models\User;
use Yii;
use app\helpers\DynamicModels;

class YourAccountController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $linkedAccounts = SocAccount::find()->where(['primary_user_id' => Yii::$app->user->id])->all();

        $userChangeNameModel = DynamicModels::getDynamicModelUserChangeName();
        $userChangeNameModel->name = Yii::$app->user->identity->username;

        $userChangeEmailModel = DynamicModels::getDynamicModelUserChangeEmail();
        $userChangeEmailModel->email = Yii::$app->user->identity->email;

        $userChangePasswordModel = DynamicModels::getDynamicModelUserChangePassword();

        if ($changeRequest = Yii::$app->request->post()) {
            if ($changeRequest['user-change-name']['checker']) {
                $user = User::findOne(Yii::$app->user->id);
                $user->username = $changeRequest['user-change-name']['field'];
                $user->save();
            }

            if ($changeRequest['user-change-email']['checker']) {
                Mail::sendChangeMail($changeRequest['user-change-email']['field'], Yii::$app->user->identity->email);
                $message = "На новый адрес почты выслано письмо со ссылкой для подтверждения смены адреса почты. 
                До момента подтверждения вход в систему будет осущетвляться с текущим адресом почты";
                Yii::$app->session->setFlash('change-mail', $message);
            }

            if ($changeRequest['user-change-password']['checker']) {
                Mail::sendChangePassword(Yii::$app->user->identity->email, $changeRequest['user-change-password']['field-1']);
                $user = User::findOne(Yii::$app->user->identity->id);
                $user->password_hash = Yii::$app->security->generatePasswordHash($changeRequest['user-change-password']['field-1']);
                $user->save();

                $message = "На текущий адрес почты (". Yii::$app->user->identity->email .") выслано письмо с новым паролем";
                Yii::$app->session->setFlash('change-password', $message);
            }

            return $this->redirect('/hunter/your-account');
        }

        return $this->render('index', compact('linkedAccounts', 'userChangeNameModel', 'userChangeEmailModel', 'userChangePasswordModel'));
    }

    public function actionAuthWithFacebook()
    {
        $userData = FacebookAuth::getUserData();

        if ($userData->warning) {
            return $this->actionFacebookReauth($userData);
        } else {
            $this->createSocUser($userData);
        }
    }

    public function actionAuthWithTwitter()
    {
        $userData = TwitterAuth::getUserData(Yii::$app->request->get('oauth_verifier'));
        if ($userData) {
            $this->createSocUser($userData);
        }
    }

    public function actionAuthWithGoogle()
    {
        if (Yii::$app->request->get('code')) {
            $code = Yii::$app->request->get('code');
            $userData = GoogleAuth::getUserData($code);
            if ($userData) {
                $this->createSocUser($userData);
            }
        }
    }

    public function actionFacebookReauth($userData)
    {
        return $this->render('facebook-reauth', compact('userData'));
    }

    private function createSocUser($userData)
    {
        $existingSocUser = SocAccount::find()->where(['soc_user_email' => $userData->soc_user_email, 'soc_network_name' => $userData->soc_network_name])->one();

        if ($existingSocUser && ($existingSocUser->primary_user_id !== Yii::$app->user->id)){
            Mail::sendLinkingRequest($existingSocUser->soc_user_email, $existingSocUser->linking_request_code, Yii::$app->user->id);
            return $this->redirect('linking-warning');
        }

        $userData->primary_user_id = Yii::$app->user->id;
        SocUser::create($userData);
        return $this->redirect(['index', '#' => 'networks']);
    }

    public function actionLinkingWarning()
    {
        return $this->render('linking-warning');
    }

    public function actionLinkingRequest()
    {
        $secret = Yii::$app->request->get('secret');
        $secret = Utils::prepareUrl($secret);
        $decoded = Yii::$app->encrypter->decrypt($secret);

        if ($decoded) {
            $params = explode('/', $decoded);
            $request = $params[0];
            $newPrimaryUserId = $params[1];

            $socUser = SocAccount::find()->where(['linking_request_code' => $request])->one();
            $newPrimaryUser = User::findOne($newPrimaryUserId);

            if (!$socUser || !$newPrimaryUser || $newPrimaryUser->role->item_name !== 'hunter') {
                return $this->render('invalid-request');
            }

            $currentPrimaryUser = $socUser->primary_user_id;

            $socUser->primary_user_id = $newPrimaryUserId;
            $socUser->save();

            $otherSocUsers = SocAccount::find()->where(['primary_user_id' => $currentPrimaryUser])->all();

            if (!$otherSocUsers) {
                User::deleteAll(['id' => $currentPrimaryUser]);
            }

            return $this->redirect(['index', '#' => 'networks']);
        } else {
            return $this->render('invalid-request');
        }
    }

    public function actionEmailChanging()
    {
        $secret = Yii::$app->request->get('secret');
        $secret = Utils::prepareUrl($secret);
        $decoded = Yii::$app->encrypter->decrypt($secret);

        if ($decoded) {
            $params = explode('/', $decoded);
            $newMail = $params[0];
            $oldMail = $params[1];
            $user = User::find()->where(['email' => $oldMail])->one();
            $user->email = $newMail;
            $user->save();
            return $this->redirect('/hunter/your-account');
        } else {
            return $this->render('invalid-request');
        }
    }
}
