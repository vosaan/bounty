<?php

namespace app\modules\hunter\controllers;

use yii\web\Controller;

/**
 * Default controller for the `hunter` module
 */
class DashboardController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
