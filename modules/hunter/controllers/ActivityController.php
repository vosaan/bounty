<?php

namespace app\modules\hunter\controllers;

use app\helpers\BountyProgramHlp;
//use app\models\RelHunterProgram;
//use app\models\SocAccount;
use app\helpers\Utils;
use Yii;
use app\models\BountyProgramAction;
use app\models\RelHunterProgram;
use yii\helpers\ArrayHelper;

use app\models\TwitterPost;

class ActivityController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $tasks = [];
        $hunterPrograms = RelHunterProgram::find()
            ->select(['program_id'])
            ->where(['hunter_primary_user_id' => Yii::$app->user->id])
            ->asArray()->all();

        //Utils::fOut(ArrayHelper::map($hunterPrograms, 'program_id', 'program_id'));

        $posts = TwitterPost::find()
            ->where(['program_id' => ArrayHelper::map($hunterPrograms, 'program_id', 'program_id')])
            ->all();

        $i = 0;
        foreach ($posts as $post) {
            $networkName = 'twitter';
            $actions = BountyProgramAction::find()->where(['module_id' => $post['program_module_id']])->all();
            foreach ($actions as $action) {
                $tasks[$i]['post_id'] = $post->post_id;
                $tasks[$i]['action'] = $action->action->name;
                $tasks[$i]['network'] = $networkName;

                $tasks[$i]['stake'] = BountyProgramHlp::getStake(
                    Yii::$app->user->id,
                    $networkName,
                    $action->id
                );
                $i++;
            }
        }

//        \app\helpers\Utils::fOut($tasks,1);
//        $hunterPrograms = RelHunterProgram::find()
//            ->where(['hunter_primary_user_id' => Yii::$app->user->id])
//            ->all();
//
//        //Utils::fOut($hunterPrograms);
//
//        $programs = [];
//
//        foreach ($hunterPrograms as $programKey => $hunterProgram){
//            $programs[$programKey]['name'] = $hunterProgram->program->name;
//            $programs[$programKey]['description'] = $hunterProgram->program->description;
//
//            $programModules = $hunterProgram->program->bountyProgramModules;
//            $modules = [];
//            foreach ($programModules as $moduleKey => $programModule){
//                $modules[$moduleKey]['name'] = $programModule->module->name;
//                $modules[$moduleKey]['module_target_link'] = $programModule->module_target_link;
//                $modules[$moduleKey]['socUserId'] = SocAccount::find()->where(['soc_network_name' => strtolower($programModule->module->name)])->one()->soc_user_id;
//
//                $moduleActions = $programModule->moduleActions;
//                $actions = [];
//                foreach ($moduleActions as $actionKey => $moduleAction) {
//                    $actions[$actionKey]['name'] = $moduleAction->action->name;
//                    $actions[$actionKey]['id'] = $moduleAction->action_id;
//                    $actions[$actionKey]['stake'] = BountyProgramHlp::getStake(
//                        Yii::$app->user->id,
//                        $programModule->module->name,
//                        $moduleAction->id
//                    );
//
//                }
//                $modules[$moduleKey]['actions'] = $actions;
//            }
//            $programs[$programKey]['modules'] = $modules;
//        }

        return $this->render('index', compact('tasks'));


//        $myTasks = HunterTask::find()->where(['hunter_id' => Yii::$app->user->id])->all();
//        $socAccounts =  SocAccount::find()->where(['primary_user_id' => Yii::$app->user->id])->asArray()->all();
//        return $this->render('index', compact('myTasks', 'socAccounts'));
    }

}
