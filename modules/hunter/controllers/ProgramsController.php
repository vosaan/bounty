<?php

namespace app\modules\hunter\controllers;

use Yii;
use app\models\BountyProgram;
use app\models\BountyProgramSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\RelHunterProgram;

/**
 * ProgramsController implements the CRUD actions for BountyProgramHlp model.
 */
class ProgramsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'join' => ['POST'],
                    'leave' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BountyProgramHlp models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BountyProgramSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $hunterPrograms = RelHunterProgram::find()
            ->select('program_id')
            ->where(['hunter_primary_user_id' => Yii::$app->user->id])
            ->column();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'hunterPrograms' => $hunterPrograms
        ]);
    }

    /**
     * Displays a single BountyProgramHlp model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $hunterPrograms = RelHunterProgram::find()
            ->select('program_id')
            ->where(['hunter_primary_user_id' => Yii::$app->user->id])
            ->column();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'hunterPrograms' => $hunterPrograms
        ]);
    }

    public function actionJoin($id){
        $programForJoin = new RelHunterProgram();
        $programForJoin->program_id = $id;
        $programForJoin->hunter_primary_user_id = Yii::$app->user->id;
        if ($programForJoin->validate()) $programForJoin->save();

        return $this->redirect('index');
    }

    public function actionLeave($id){
        RelHunterProgram::deleteAll(['program_id' => $id, 'hunter_primary_user_id' => Yii::$app->user->id]);

        return $this->redirect('index');
    }


    /**
     * Finds the BountyProgramHlp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BountyProgram the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BountyProgram::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
