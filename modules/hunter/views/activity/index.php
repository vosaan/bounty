<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
?>
<h1>Your activities</h1>


<?php foreach ($tasks as $task) : ?>
    <?php
        $action = '';
        if ($task['action'] == 'Like') {
            $action = 'like';
        } elseif ($task['action'] == 'Repost') {
            $action = 'retweet';
        }
    ?>
    <div class="task-container task-<?=$task['network']?>" id="<?=$action . '-' . $task['post_id']?>">
        <?php if ($task['action'] == 'Like') : ?>
            <a class="action-link" href="https://twitter.com/intent/like?tweet_id=<?=$task['post_id']?>">Like</a>
        <?php elseif ($task['action'] == 'Repost') : ?>
            <a class="action-link" href="https://twitter.com/intent/retweet?tweet_id=<?=$task['post_id']?>">Retweet</a>
        <?php endif;?>
        <p>Your stake: <?=$task['stake'] ? : 0 ?></p>
    </div>
<?php endforeach;?>

<script>
    function reward_user( event ) {
        if ( event ) {
            alert( 'Tweeted' );
            console.log( event );
        }
    }

    window.twttr = (function (d,s,id) {
        var t, js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
        js.src="//platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);
        return window.twttr || (t = { _e: [], ready: function(f){
            t._e.push(f) }
        }
        );
    }(document, "script", "twitter-wjs"));

    twttr.ready(function (twttr) {
        twttr.events.bind('tweet', reward_user);
    });
</script>