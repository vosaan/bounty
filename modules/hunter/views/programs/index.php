<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\helpers\BountyProgramHlp;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BountyProgramSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bounty Programs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bounty-program-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'name',
                'value' => function($model){return Html::a($model->name, ['view', 'id' => $model->id]);},
                'format' => 'raw'
            ],
            //'description:ntext',
            //'status_id',
            //'owner_id',
            [
                'attribute' => 'owner_id',
                'value' => 'owner.username',
                'label' => 'Owner'
            ],
            'term_start',
            'term_stop',
            //'program_type_id',
            'common_budget',
            //'token_coefficient',
            //'currency_id',
            //'official_site_link',
            //'bitcoin_talk_thread_link',
            //'bitcoin_talk_bounty_program_link',
            //'coefficient_currency_id',
            [
                'attribute' => 'joined',
                'label' => 'Joined',
                'value' => function($model) use ($hunterPrograms){
                    return (in_array($model->id, $hunterPrograms)) ? 'Joined' : 'Not joined';
                }
            ],

            [
                //'attribute' => 'bountyProgramModules',
                'label' => 'Modules',
                'value' => function($model){
                    return BountyProgramHlp::getProgramModules($model);
                },
                'filter' => \app\models\DirModule::find()->select(['name', 'id'])->indexBy('id')->column(),
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
