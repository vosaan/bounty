<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RelHunterProgram */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rel-hunter-program-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'hunter_primary_user_id')->textInput() ?>

    <?= $form->field($model, 'program_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
