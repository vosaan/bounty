<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DirAction */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Available programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dir-action-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            'rules:html'
        ],
    ]) ?>

    <?php
    foreach ($model->bountyProgramModules as $programModule) {
        echo '<h4>' . $programModule->module->name . '</h4>';
        echo '<p>' . $programModule->module_target_link . '</p>';
        foreach ($programModule->moduleActions as $moduleAction) {
            \app\helpers\Utils::fOut($moduleAction->action->name);
        }

        echo '<hr>';
    }
    ?>

    <?php //\app\helpers\Utils::fOut($model->bountyProgramModules)?>

    <?php echo in_array($model->id, $hunterPrograms) ?
        Html::a('Leave this program', ['leave', 'id' => $model->id], [
            'class' => 'btn btn-warning',
            'data' => [
                'confirm' => 'Are you sure you want to leave this program?',
                'method' => 'post',
            ],
        ])
        :
        Html::a('Join this program', ['join', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'method' => 'post',
            ],
        ])
    ?>
</div>