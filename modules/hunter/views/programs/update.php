<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RelHunterProgram */

$this->title = 'Update Rel Hunter Program: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Rel Hunter Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rel-hunter-program-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
