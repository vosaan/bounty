<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RelHunterProgram */

$this->title = 'Create Rel Hunter Program';
$this->params['breadcrumbs'][] = ['label' => 'Rel Hunter Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rel-hunter-program-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
