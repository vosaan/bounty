<?php
use app\helpers\Utils;
use app\helpers\SocUser;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use kartik\form\ActiveForm;
//use kartik\form\ActiveField



?>

<div class="row">
    <div class="col-lg-8 col-lg-offset-2">
        <h2>Account</h2>

        <ul class="nav nav-tabs" id="user-tabs">
            <li class="active"><a data-toggle="tab" href="#profile">Profile</a></li>
            <li><a data-toggle="tab" href="#networks">Networks</a></li>
            <li><a data-toggle="tab" href="#email">Email notifications</a></li>
            <li><a data-toggle="tab" href="#security">Security</a></li>
        </ul>

        <div class="tab-content">
            <div id="profile" class="tab-pane fade in active">

                <!-- Смена имени пользователя -->
                <h4>Change your username</h4>
                <?php $form = ActiveForm::begin()?>
                <div class="row">
                    <div class="col-lg-8">
                        <?=$form->field($userChangeNameModel, 'name')->textInput(
                            [
                                'disabled' => true,
                                'id' => 'user-change-name-field',
                                'name' => 'user-change-name[field]',
                            ]
                        )?>
                    </div>
                    <div class="col-lg-4">
                        <?=Html::checkbox('user-change-name[checker]', false,
                            [
                                'label' => 'Change',
                                'id' => 'user-change-name-checker',
                            ]
                        )?>
                    </div>
                </div>
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?php ActiveForm::end()?>
                <!-- /Смена имени пользователя -->
                <hr>

                <!-- Смена адреса почты -->
                <h4>Change your email</h4>
                <?php if (Yii::$app->session->hasFlash('change-mail')) : ?>
                    <div class="panel panel-danger" id="mail-change">
                        <div class="panel-heading">Внимание!
                            <button type="button" class="close" data-target="#mail-change" data-dismiss="alert">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                        </div>
                        <div class="panel-body">
                            <?=Yii::$app->session->getFlash('change-mail')?>
                        </div>
                    </div>
                <?php endif;?>
                <?php $form = ActiveForm::begin()?>
                <div class="row">
                    <div class="col-lg-8">
                        <?=$form->field($userChangeEmailModel, 'email')->textInput(
                            [
                                'disabled' => true,
                                'id' => 'user-change-email-field',
                                'name' => 'user-change-email[field]',
                            ]
                        )?>
                    </div>
                    <div class="col-lg-4">
                        <?=Html::checkbox('user-change-email[checker]', false,
                            [
                                'label' => 'Change',
                                'id' => 'user-change-email-checker',
                            ]
                        )?>
                    </div>
                </div>
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?php ActiveForm::end()?>
                <!-- /Смена адреса почты -->
                <hr>

                <!-- Смена пароля -->
                <h4>Change your password</h4>
                <?php if (Yii::$app->session->hasFlash('change-password')) : ?>
                    <div class="panel panel-success" id="password-change">
                        <div class="panel-heading">Внимание!
                            <button type="button" class="close" data-target="#password-change" data-dismiss="alert">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                        </div>
                        <div class="panel-body">
                            <?=Yii::$app->session->getFlash('change-password')?>
                        </div>
                    </div>
                <?php endif;?>
                <?php $form = ActiveForm::begin()?>
                <div class="row">
                    <div class="col-lg-8">
                        <?=$form->field($userChangePasswordModel, 'password')->passwordInput(
                            [
                                'disabled' => true,
                                'id' => 'user-change-password-field-1',
                                'name' => 'user-change-password[field-1]',
                            ]
                        )?>

                        <?=$form->field($userChangePasswordModel, 'confirmPassword')->passwordInput(
                            [
                                'disabled' => true,
                                'id' => 'user-change-password-field-2',
                                'name' => 'user-change-password[field-2]',
                            ]
                        )?>
                    </div>
                    <div class="col-lg-4">
                        <?=Html::checkbox('user-change-password[checker]', false,
                            [
                                'label' => 'Change',
                                'id' => 'user-change-password-checker',
                            ]
                        )?>
                    </div>
                </div>
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?php ActiveForm::end()?>
                <!-- /Смена пароля -->

            </div>
            <div id="networks" class="tab-pane fade">
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <table class="table table-striped">
                    <?= SocUser::getNetworksTable($linkedAccounts)?>
                </table>
            </div>
            <div id="email" class="tab-pane fade">
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
            </div>
            <div id="security" class="tab-pane fade">
                <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
            </div>
        </div>
    </div>
</div>