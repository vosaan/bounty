<p class="alert alert-danger">Вы пытаетесь присоединить аккаунт социальной сети, который, возможно, вам не принадлежит. Чтобы подтвердить
    присоединение аккаунта, перейдите по ссылке, указанной в письме, которое выслано по адресу, который был указан при
    регистрации аккаунта социальной сети.</p>