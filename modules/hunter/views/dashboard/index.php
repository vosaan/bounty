<?php
use yii\helpers\Html;
?>
<div class="hunter-default-index">
    <h1>Hello Hunter</h1>
    <h3>Dashboard</h3>

    <?php if (!Yii::$app->user->identity->email) : ?>
    <div class="panel panel-danger copyright-wrap" id="copyright-wrap">
        <div class="panel-heading">Attention!
            <button type="button" class="close" data-target="#copyright-wrap" data-dismiss="alert">
                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
            </button>
        </div>
        <div class="panel-body">
            For more effective work, we recommend <?=Html::a('filling out your profile data', '/hunter/your-account')?>!
        </div>
    </div>
    <?php endif; ?>
</div>
