<?php

namespace app\modules\owner\controllers;

use app\helpers\Utils;
use app\helpers\BountyProgramHlp;
use app\models\BountyProgramActionReward;
use app\models\BountyProgramModule;
use app\models\DirModule;
use app\models\DirProgramStatus;
use app\models\RelModuleAction;
use app\models\RelProgramTypeModule;
use Yii;
use app\models\BountyProgram;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\BountyProgramAction;
use app\models\DirRewardLevel;

/**
 * BountyProgramController implements the CRUD actions for BountyProgram model.
 */
class BountyProgramController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BountyProgram models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BountyProgram::find()->where(['owner_id' => Yii::$app->user->id]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BountyProgram model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = BountyProgram::find()->where(['id' => $id])->with(['owner', 'status', 'bountyProgramActions', 'bountyProgramModules', 'programType'])->one();

        $session = Yii::$app->session;
        if (isset($session['model'])) {
            $session->remove('model');
        }

        if ($model) {
            return $this->render('view', [
                'model' => $model,
            ]);
        } else {
            $this->redirect('index');
        }
    }

    /**
     * Creates a new BountyProgram model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $program = new BountyProgram();
        $program->status_id = DirProgramStatus::find()->where(['name' => 'Pending'])->one()->id;
        $program->owner_id = Yii::$app->user->id;
        $program->isNewRecord = true;

        if ($program->load(Yii::$app->request->post()) && $program->validate()) {
            $session = Yii::$app->session;
            $session->set('program', $program);
            return $this->redirect('create-modules');
        }

        return $this->render('create', [
            'model' => $program,
        ]);
    }

    /**
     * Updates an existing BountyProgram model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $program = $this->findModel($id);
        $program->isNewRecord = false;

        if ($program->load(Yii::$app->request->post()) && $program->validate()) {
            $session = Yii::$app->session;
            $session->set('program', $program);

            return $this->redirect(['update-modules', 'program_id' => $program->id]);
        }

        return $this->render('update', [
            'model' => $program,
        ]);
    }

    /**
     * Activates Bounty program
     * @param $id
     * @return \yii\web\Response
     */
    public function actionActivate($id)
    {
        $program = $this->findModel($id);
        $program->status_id = DirProgramStatus::find()->where(['name' => 'Active'])->one()->id;
        $program->save();
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing BountyProgram model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Create Modules
     * @return string|\yii\web\Response
     */
    public function actionCreateModules()
    {
        if (Yii::$app->session->isActive) {
            $session = Yii::$app->session;

            /* Если в сессии есть модель bounty-программы из предыдущего действия */
            if (isset($session['program'])) {
                $program = $session->get('program');

                /* Получаем все возможные модули для программы вместе с действиями и наградами */
                $programModules = BountyProgramHlp::getAvailableModules($program->program_type_id);

                /* Если не выбран ни один модуль */
                if (Yii::$app->request->post() && !Yii::$app->request->post('modules')) {
                    $program->save();
                    return $this->redirect(['view', 'id' => $program->id]);
                }

                /* Если выбран хотя бы один модуль */
                if ($formData = Yii::$app->request->post('modules')) {
                    $program->save();
                    BountyProgramHlp::saveModulesData($formData, $program->id);
                    return $this->redirect(['view', 'id' => $program->id]);
                }
                return $this->render('modules-form', compact('programModules','program'));
            }
        }
    }

    /**
     * Update Modules
     * @return string|\yii\web\Response
     */
    public function actionUpdateModules()
    {
        if (Yii::$app->session->isActive) {
            $session = Yii::$app->session;
            if (isset($session['program'])) {
                $program = $session->get('program');

                /* Получаем все возможные модули для программы вместе с действиями и наградами (в том числе и текущие)*/
                $programModules = BountyProgramHlp::getCurrentModules($program->program_type_id, $program->id);

                /* Если не выбран ни один модуль */
                if (Yii::$app->request->post() && !Yii::$app->request->post('modules')) {
                    $program->save();
                    BountyProgramModule::deleteAll(['program_id' => $program->id]);
                    return $this->redirect(['view', 'id' => $program->id]);
                }

                if ($formData = Yii::$app->request->post('modules')) {

                    $program->save();
                    BountyProgramModule::deleteAll(['program_id' => $program->id]);
                    BountyProgramHlp::saveModulesData($formData, $program->id);
                    return $this->redirect(['view', 'id' => $program->id]);
                }
                return $this->render('modules-form', compact('programModules', 'program'));
            }
        }
    }

    /**
     * Finds the BountyProgram model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BountyProgram the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BountyProgram::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Deletes all modules (and modules actions via foreign keys relations) of existing Bounty program
     * @param $programId
     */
    private function findAndDeleteOldProgramModules($programId)
    {
        $oldModules = BountyProgramModule::findAll(['program_id' => $programId]);
        if ($oldModules) {
            foreach ($oldModules as $module) {
                $module->delete();
            }
        }
    }

    /**
     * Returns all modules and all modules actions of existing Bounty program
     * @param $model
     * @return array
     */
    private function getCurrentProgramModules($model)
    {
        $modules = [];
        $moduleCount = 0;
        foreach ($model->bountyProgramModules as $bountyProgramModule) {
            $modules[$bountyProgramModule->module->id]['budget'] = $bountyProgramModule->module_budget;
            $modules[$bountyProgramModule->module->id]['link'] = $bountyProgramModule->module_target_link;
            $modules[$bountyProgramModule->module->id]['current_module_status'] = $bountyProgramModule->current_module_status;

            $modules[$bountyProgramModule->module->id]['actions'] = [];
            $moduleActions = $bountyProgramModule->moduleActions;
            foreach ($moduleActions as $action) {
                $modules[$bountyProgramModule->module->id]['actions'][] = $action->action_id;
            }

            $moduleCount++;
        }

        return $modules;
    }
}
