<?php

namespace app\modules\owner;

/**
 * owner module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\owner\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->layout = 'owner';

        // custom initialization code goes here
    }
}
