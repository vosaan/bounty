<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BountyProgram */

$this->title = 'Update Bounty Program: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bounty Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bounty-program-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'availableModules' => $availableModules,
        'currentProgramModules' => $currentProgramModules
    ]) ?>

</div>
