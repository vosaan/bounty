<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BountyProgram */

$this->title = 'Create Bounty Program';
$this->params['breadcrumbs'][] = ['label' => 'Bounty Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bounty-program-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'availableModules' => $availableModules,
    ]) ?>

</div>
