<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DateRangePicker;
use app\helpers\Utils;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\BountyProgram */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bounty-program-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'rules')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'custom',
        'clientOptions' => [
            'toolbarGroups' => [
                ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup' ]],
                ['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align']],
                ['name' => 'links'],
            ],
        ],
    ]) ?>

    <?php if (!$model->isNewRecord) : ?>
    <?= $form->field($model, 'status_id')->dropDownList(ArrayHelper::map(\app\models\DirProgramStatus::find()->all(), 'id', 'name'), []) ?>
    <?php endif; ?>

    <?= $form->field($model, 'term_start')->widget(DateRangePicker::className(), [
        'attributeTo' => 'term_stop',
        'form' => $form, // best for correct client validation
        'language' => 'en',
        'size' => 'lg',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ])->label('Activity period'); ?>

    <?= $form->field($model, 'program_type_id')->radioList(ArrayHelper::map(\app\models\DirProgramType::find()->all(), 'id', 'name'), []) ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'common_budget')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-9 token-coefficient-container">
                    <?= $form->field($model, 'token_coefficient')->textInput(['class' => 'form-control token-coefficient']) ?>
                </div>
                <div class="col-sm-3 currency-dropdown-container">
                    <?= $form->field(
                        $model,
                        'coefficient_currency_id')->
                    dropDownList(
                        ArrayHelper::map(
                            \app\models\DirCoefficientCurrency::find()->all(),
                            'id', 'name'
                        ),
                        ['class' => 'form-control currency-dropdown']
                    )->label('Currency') ?>
                </div>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'official_site_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bitcoin_talk_thread_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bitcoin_talk_bounty_program_link')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Next', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

