<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bounty Programs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bounty-program-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bounty Program', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            //'description:ntext',
            [
                'attribute' => 'status_id',
                'label' => 'Status',
                'value' => 'status.name'
            ],
            //'owner_id',
            //'term_start',
            //'term_stop',
            //'program_type_id',
            //'common_budget',
            //'token_coefficient',
            //'official_site_link',
            //'bitcoin_talk_thread_link',
            //'bitcoin_talk_bounty_program_link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
