<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\helpers\Utils;

/* @var $this yii\web\View */
/* @var $model app\models\BountyProgram */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bounty Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bounty-program-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            [
                'attribute' => 'status_id',
                'value' => function($model){
                    return $model->status->name;
                },
                'label' => 'Status'
            ],

            [
                'attribute' => 'owner_id',
                'value' => function($model){
                    return $model->owner->username;
                },
                'label' => 'Owner'
            ],

            'term_start',
            'term_stop',
            [
                'attribute' => 'program_type_id',
                'value' => function($model){
                    return $model->programType->name;
                },
                'label' => 'Program type'
            ],
            'common_budget',
            'token_coefficient',
            'official_site_link',
            'bitcoin_talk_thread_link',
            'bitcoin_talk_bounty_program_link',
            [
                'attribute' => 'bountyProgramModules',
                'value' => function($model){
                    $availableModules = $model->bountyProgramModules;

                    $content = '';
                    foreach ($availableModules as $module) {
                        $content .= '<div class="">' . $module->module->name . '</div>';
                        $content .= '<div>' . $module->module_target_link . '</div>';
                        $content .= '<div>' . $module->module_budget . '</div>';
                        $moduleActions = $module->moduleActions;
                        foreach ($moduleActions as $action) {
                            $content .= $action->action->name . '&nbsp&nbsp&nbsp&nbsp';
                        }

                    }

                    return $content;
                },
                'label' => 'Available modules',
                'format' => 'raw'

            ],
        ],
    ]) ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?php if ($model->status->name !== 'Active') : ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>

            <?= Html::a('Activate',
                ['activate', 'id' => $model->id],
                [
                    'class' => 'btn btn-success',
                    'disabled' => ($model->status->name !== 'Active' && !$model->bountyProgramModules) ? true : false
                ]
            )
            ?>
        <?php endif; ?>
    </p>
    <p><?=  !$model->bountyProgramModules ? 'You must set at least one module to activate your bounty program' : ''?></p>

</div>
