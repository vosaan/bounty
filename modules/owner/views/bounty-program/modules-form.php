<?php
/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;
?>

<?php (integer)$modulesBudget = 0;?>
<?php $form = ActiveForm::begin(
    [
        'id' => 'modules-form',
        'options' => [
            'data-controller-action' => $program->isNewRecord ? 'create' : 'update',
            'data-program-status' => strtolower($program->status->name)
        ]
    ]
) ?>
    <?php foreach ($programModules as $moduleKey => $module) : ?>
        <?php $modulesBudget += $module->module_budget;?>
        <div class="module-container">
            <?=Html::checkbox('modules['.$moduleKey.'][module_id]', $module->program_id, //Свойство "checked". Если модуль уже
                // принадледит какой-то программе (при редактировании), будет true.
                [
                    'label' => $module->module->name,
                    'class' => 'module-switcher',
                    'value' => $module->module_id
                ]
            )?>
            <div class="module-data">
                <div class="row">
                    <div class="col-lg-8">
                        <?=$form->field($module, 'module_target_link')->textInput(
                            [
                                'name' => 'modules['.$moduleKey.'][module_target_link]',
                                'id' => 'module-target-'.$moduleKey,
                                'class' => 'module-target-link form-control'
                            ]
                        )?>
                    </div>
                    <div class="col-lg-4">
                        <?=$form->field($module, 'module_budget')->textInput(
                            [
                                'name' => 'modules['.$moduleKey.'][module_budget]',
                                'id' => 'module-budget-'.$moduleKey,
                                'class' => 'module-budget form-control'
                            ]
                        )?>
                    </div>
                </div>
                <div class="module-actions">
                    <?php foreach ($module->actions as $actionKey => $action) : ?>
                        <div class="action-container">

                            <?php //Utils::fOut($action)?>

                            <?=Html::checkbox('modules['.$moduleKey.'][actions]['.$actionKey.'][action_id]', $action->module_id, //Свойство "checked". Если действие уже
                                // принадледит какому-то модулю (при редактировании), будет true.
                                [
                                    'label' => $action->action->name,
                                    'class' => 'action-switcher',
                                    'value' => $action->action_id,
                                ]
                            )?>
                            <div class="action-data">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <?= $form->field($action, 'deadline')->widget(
                                            DatePicker::className(), [
                                            'options' => [
                                                'id' => 'module-'.$moduleKey.'-actions-'.$actionKey.'-deadline',
                                                'name' => 'modules['.$moduleKey.'][actions]['.$actionKey.'][deadline]',
                                                'data-function' => 'deadline-date-picker'
                                            ],
                                            'template' => '{addon}{input}',
                                            'clientOptions' => [
                                                'autoclose' => true,
                                                'format' => 'yyyy-mm-dd',
                                                'startDate' => date('Y:m:d'),
                                            ]
                                        ]);?>
                                    </div>
                                    <div class="col-lg-9">
                                        <?php foreach ($action->rewards as $reward) : ?>
                                            <div class="row action-slider-container">
                                                <div class="col-lg-2">
                                                    <label class="control-label"><?=$reward->rewardLevel->name?></label>
                                                </div>
                                                <div class="col-lg-6">
                                                    <?=$form->field($reward, 'followers_count')->textInput(
                                                        [
                                                            'name' => 'modules['.$moduleKey.'][actions]['.$actionKey.'][rewards]['.$reward->reward_level_id.'][followers_count]',
                                                            'id' => 'followers-slider-modules-'.$moduleKey.'-actions-'.$actionKey.'-rewards-'.$reward->reward_level_id.'-followers_count',
                                                            'data-slider-min' => 0,
                                                            'data-slider-max' => 5000,
                                                            'data-slider-step' => 50,
                                                            'data-slider-value' => $reward->followers_count ? : 200,
                                                            'class' => 'followers-count-slider form-control'
                                                        ]
                                                    )?>
                                                </div>
                                                <div class="col-lg-2">
                                                    <?=$form->field($reward, 'reward_value')->textInput(
                                                        [
                                                            'name' => 'modules['.$moduleKey.'][actions]['.$actionKey.'][rewards]['.$reward->reward_level_id.'][reward_value]',
                                                            'id' => 'modules-'.$moduleKey.'-actions-'.$actionKey.'-rewards-'.$reward->reward_level_id.'-reward_value',
                                                            'value' => $reward->reward_value ? : 1,
                                                            'class' => 'reward-value form-control',
                                                        ]
                                                    )?>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <?=Html::submitButton('Save', ['class' => 'btn btn-primary', 'id' => 'save-modules'])?>

    <!-- Счётчик остатка токенов -->
    <div class="common-program-budget-container">
        <div class="common-program-budget" data-common-budget="<?= $program->common_budget ?>">
            <?=(integer)$program->common_budget - $modulesBudget?>
        </div>
        <h4>tokens left</h4>
        <?= Html::hiddenInput('tokenscounter', ((integer)$program->common_budget - $modulesBudget), ['class' => 'tokens-counter'])?>
    </div>


<?php ActiveForm::end() ?>

