<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DirModule */

$this->title = 'Update Dir Module: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Dir Modules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dir-module-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'actions' => $actions
    ]) ?>

</div>
