<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\DirModule */
/* @var $form yii\widgets\ActiveForm */

$currentActions = [];
foreach ($model->actions as $action) {
    $currentActions[] = $action->action_id;
}
?>

<div class="dir-module-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 1 => 'Active', 0 => 'Not active']) ?>

    <?= $form->field($model, 'module_type_id')->dropDownList(ArrayHelper::map(\app\models\DirModuleType::find()->all(), 'id', 'name')) ?>

    <label class="control-label">Module actions</label>
    <div class="module-actions">
        <?php foreach ($actions as $id=>$action) : ?>
            <?php if (in_array($id, $currentActions)) : ?>
                <?= Html::checkbox('actions['. $id . ']', true, ['value' => $id, 'label' => $action])?>
            <?php else : ?>
                <?= Html::checkbox('actions['. $id . ']', false, ['value' => $id, 'label' => $action])?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
