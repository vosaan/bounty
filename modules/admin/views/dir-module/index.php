<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Modules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dir-module-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Module', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'description:ntext',
            [
                'attribute' => 'status',
                'value' => function($model){
                    return $model->status ? 'Active' : 'Not active';
                }
            ],

            [
                'attribute' => 'module_type_id',
                'label' => 'Module type',
                'value' => function($model){
                    $moduleType = \app\models\DirModuleType::find()->where(['id' => $model->module_type_id])->one();
                    return $moduleType->name;
                }
            ],
            [
                'attribute' => 'actions',
                'label' => 'Actions',
                'value' => function($model){
                    $actionsArr = [];
                    foreach ($model->actions as $action) {
                        $actionsArr[] = $action->action->name;
                    }

                    return implode(', ', $actionsArr);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
