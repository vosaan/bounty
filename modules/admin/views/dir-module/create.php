<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DirModule */

$this->title = 'Create Dir Module';
$this->params['breadcrumbs'][] = ['label' => 'Dir Modules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dir-module-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'actions' => $actions
    ]) ?>

</div>
