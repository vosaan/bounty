<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DirProgramType */

$this->title = 'Create Dir Program Type';
$this->params['breadcrumbs'][] = ['label' => 'Dir Program Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dir-program-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modules' => $modules,
    ]) ?>

</div>
