<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DirProgramType */

$this->title = 'Update Dir Program Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Dir Program Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dir-program-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modules' => $modules,
    ]) ?>

</div>
