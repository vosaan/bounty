<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DirProgramType */
/* @var $form yii\widgets\ActiveForm */

$currentModules = [];
foreach ($model->programModules as $module) {
    $currentModules[] = $module->module_id;
}
?>

<div class="dir-program-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 1 => 'Active', 0 => 'Not active']) ?>

    <p class="control-label">Modules:</p>
    <div class="module-actions">
        <?php foreach ($modules as $id => $name) : ?>
            <?php if (in_array($id, $currentModules)) : ?>
                <div>
                    <?= Html::checkbox('modules[' . $id . ']', true, ['value' => $id, 'label' => $name])?>
                </div>
            <?php else : ?>
                <div>
                    <?= Html::checkbox('modules[' . $id . ']', false, ['value' => $id, 'label' => $name])?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
