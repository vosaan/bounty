<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DirAction */

$this->title = 'Create Dir Action';
$this->params['breadcrumbs'][] = ['label' => 'Dir Actions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dir-action-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
