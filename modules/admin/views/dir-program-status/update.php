<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DirProgramStatus */

$this->title = 'Update Dir Program Status: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Dir Program Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dir-program-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
