<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DirProgramStatus */

$this->title = 'Create Dir Program Status';
$this->params['breadcrumbs'][] = ['label' => 'Dir Program Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dir-program-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
