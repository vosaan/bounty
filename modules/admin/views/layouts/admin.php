<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\helpers\Utils;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\HunterAsset;

HunterAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => 'Bounty',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);

//        $menuItems[] = [
//            'label' => 'Users',
//            'items' => [
//                ['label' => 'Hunters', 'url' => '#'],
//                '<li class="divider"></li>',
//                ['label' => 'Owners', 'url' => '#'],
//                '<li class="divider"></li>',
//                ['label' => 'Admins', 'url' => '#'],
//            ],
//        ];

        $menuItems[] = [
            'label' => 'Bounty',
            'items' => [
                ['label' => 'Bounty program statuses', 'url' => '/admin/dir-program-status'],
                '<li class="divider"></li>',
                ['label' => 'Bounty program types', 'url' => '/admin/dir-program-type'],
                '<li class="divider"></li>',
                ['label' => 'Bounty program actions', 'url' => '/admin/dir-action'],
                '<li class="divider"></li>',
                ['label' => 'Bounty module types', 'url' => '/admin/dir-module-type'],
                '<li class="divider"></li>',
                ['label' => 'Bounty modules', 'url' => '/admin/dir-module'],
                '<li class="divider"></li>',
                ['label' => 'Currency', 'url' => '/admin/dir-coefficient-currency'],
                '<li class="divider"></li>',
                ['label' => 'Reward level', 'url' => '/admin/dir-reward-level'],
            ],
        ];

        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                //'Logout',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
        NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>