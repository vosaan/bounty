<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DirCoefficientCurrency */

$this->title = 'Update Dir Coefficient Currency: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Dir Coefficient Currencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dir-coefficient-currency-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
