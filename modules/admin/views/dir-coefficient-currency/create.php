<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DirCoefficientCurrency */

$this->title = 'Create Dir Coefficient Currency';
$this->params['breadcrumbs'][] = ['label' => 'Dir Coefficient Currencies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dir-coefficient-currency-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
