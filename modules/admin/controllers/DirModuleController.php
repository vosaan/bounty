<?php

namespace app\modules\admin\controllers;

use app\helpers\Utils;
use Yii;
use app\models\DirModule;
use app\models\DirAction;
use app\models\RelModuleAction;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
/**
 * DirModuleController implements the CRUD actions for DirModule model.
 */
class DirModuleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DirModule models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => DirModule::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DirModule model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DirModule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DirModule();
        $actions = ArrayHelper::map(DirAction::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $actions = Yii::$app->request->post('actions');
            if ($actions) {
                foreach ($actions as $action) {
                    $RelModuleAction = new RelModuleAction();
                    $RelModuleAction->module_id = $model->id;
                    $RelModuleAction->action_id = $action;
                    $RelModuleAction->save();
                }
            }

            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'actions' => $actions
        ]);
    }

    /**
     * Updates an existing DirModule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = DirModule::find()->where(['id' => $id])->with('actions')->one();
        $actions = ArrayHelper::map(DirAction::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $newActions = Yii::$app->request->post('actions');

            /* Если $newActions пуст, значит сняты все галочки */
            if (!$newActions) {

                /* нужно найти и удалить все предыдущие действия (если они были) */
                $this->findAndDeleteOldModuleActions($model->id);
            } else {
                /* нужно найти и удалить все предыдущие действия (если они были) */
                $this->findAndDeleteOldModuleActions($model->id);

                /* и записать новые */
                foreach ($newActions as $action) {
                    $RelModuleAction = new RelModuleAction();
                    $RelModuleAction->module_id = $model->id;
                    $RelModuleAction->action_id = $action;
                    $RelModuleAction->save();
                }
            }

            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'actions' => $actions
        ]);
    }

    /**
     * Deletes an existing DirModule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DirModule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DirModule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DirModule::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function findAndDeleteOldModuleActions($moduleId)
    {
        $oldActions = RelModuleAction::findAll(['module_id' => $moduleId]);
        if ($oldActions) {
            foreach ($oldActions as $action) {
                $oldAction = RelModuleAction::find()->where(['module_id' => $action->module_id, 'action_id' => $action->action_id])->one();
                $oldAction->delete();
            }
        }
    }

}
