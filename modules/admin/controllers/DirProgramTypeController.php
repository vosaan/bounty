<?php

namespace app\modules\admin\controllers;

use app\models\RelProgramTypeModule;
use Yii;
use app\models\DirProgramType;
use app\models\DirModule;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DirProgramTypeController implements the CRUD actions for DirProgramType model.
 */
class DirProgramTypeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DirProgramType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => DirProgramType::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DirProgramType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DirProgramType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DirProgramType();
        $modules = ArrayHelper::map(DirModule::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->post('modules')) {
                foreach (Yii::$app->request->post('modules') as $module_id) {
                    $relPTypeModule = new RelProgramTypeModule();
                    $relPTypeModule->program_type_id = $model->id;
                    $relPTypeModule->module_id = $module_id;
                    $relPTypeModule->save();
                }
            }


            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'modules' => $modules,
        ]);
    }

    /**
     * Updates an existing DirProgramType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modules = ArrayHelper::map(DirModule::find()->all(), 'id', 'name');


        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            RelProgramTypeModule::deleteAll(['program_type_id' => $model->id]);
            if (Yii::$app->request->post('modules')) {
                foreach (Yii::$app->request->post('modules') as $module_id) {
                    $relPTypeModule = new RelProgramTypeModule();
                    $relPTypeModule->program_type_id = $model->id;
                    $relPTypeModule->module_id = $module_id;
                    $relPTypeModule->save();
                }
            }


            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'modules' => $modules,
        ]);
    }

    /**
     * Deletes an existing DirProgramType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DirProgramType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DirProgramType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DirProgramType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
