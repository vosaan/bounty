<?php

use yii\db\Migration;

/**
 * Handles the creation of table `reward_level`.
 */
class m180329_131798_create_dir_reward_level_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dir_reward_level}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%dir_reward_level}}',
            'Уровни наград'
        );

        $this->batchInsert('{{%dir_reward_level}}',
            [
                'id',
                'name'
            ],
            [
                [
                    '1',
                    'Level 3'
                ],
                [
                    '2',
                    'Level 2'
                ],
                [
                    '3',
                    'Level 1'
                ],
                [
                    '4',
                    'Level 0'
                ],
                [
                    '5',
                    'Extra level'
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dir_reward_level}}');
    }
}
