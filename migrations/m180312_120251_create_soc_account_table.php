<?php

use yii\db\Migration;

/**
 * Handles the creation of table `linked_account`.
 */
class m180312_120251_create_soc_account_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%soc_account}}', [
            'id' => $this->primaryKey(),
            'primary_user_id' => $this->integer(),
            'soc_network_name' => $this->string(),
            'soc_user_id' => $this->string(),
            'soc_user_name' => $this->string(),
            'soc_user_email' => $this->string(),
            'soc_user_pic' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'soc_user_followers_count' => $this->integer(),
            'soc_user_screen_name' => $this->string(),
            'linking_request_code' => $this->string()->unique()
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%soc_account}}',
            'Таблица пользователей соц. сетей'
        );

        $this->addForeignKey(
            'fk-soc_account-user',
            '{{%soc_account}}',
            'primary_user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%soc_account}}');
    }
}
