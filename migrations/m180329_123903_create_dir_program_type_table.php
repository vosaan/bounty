<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dir_program_type`.
 */
class m180329_123903_create_dir_program_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dir_program_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'status' => "ENUM('1','0') NOT NULL DEFAULT '1'"
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%dir_program_type}}',
            'Справочник типов программ: Free, Pro, Enterprice и т.д.'
        );

        $this->batchInsert('{{%dir_program_type}}',
            [
                'id',
                'name',
                'description',
                'status'
            ],
            [
                [
                    '1',
                    'Free',
                    '',
                    '1'
                ],
                [
                    '2',
                    'Pro',
                    '',
                    '1'
                ],
                [
                    '3',
                    'Enterprise',
                    '',
                    '1'
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dir_program_type}}');
    }
}
