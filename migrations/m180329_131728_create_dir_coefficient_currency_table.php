<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dir_coefficient_currency`.
 */
class m180329_131728_create_dir_coefficient_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dir_coefficient_currency}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%dir_coefficient_currency}}',
            'Таблица валют'
        );

        $this->batchInsert('{{%dir_coefficient_currency}}',
            [
                'id',
                'name'
            ],
            [
                [
                    '1',
                    'USD'
                ],
                [
                    '2',
                    'Bitcoin'
                ],
                [
                    '3',
                    'Ethereum'
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dir_coefficient_currency}}');
    }
}
