<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bounty_program`.
 */
class m180330_081223_create_bounty_program_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bounty_program}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'status_id' => $this->integer()->notNull(),
            'owner_id' => $this->integer()->notNull(),
            'term_start' => $this->date()->notNull(),
            'term_stop' => $this->date()->notNull(),
            'program_type_id' => $this->integer()->notNull(),
            'common_budget' => $this->float()->notNull(),
            'token_coefficient' => $this->float()->notNull(),
            'official_site_link' => $this->string()->notNull(),
            'bitcoin_talk_thread_link' => $this->string()->notNull(),
            'bitcoin_talk_bounty_program_link' => $this->string()->notNull(),
            'coefficient_currency_id' => $this->integer()->notNull()->defaultValue(1),
            'rules' => $this->text()->defaultValue(null)
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%bounty_program}}',
            'Таблица bounty-программ'
        );

        $this->addForeignKey(
            'f_owner_id',
            '{{%bounty_program}}',
            'owner_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'f_status_id',
            '{{%bounty_program}}',
            'status_id',
            '{{%dir_program_status}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'f_program_type_id',
            '{{%bounty_program}}',
            'program_type_id',
            '{{%dir_program_type}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'f_coefficient_currency_id',
            '{{%bounty_program}}',
            'coefficient_currency_id',
            '{{%dir_coefficient_currency}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bounty_program}}');
    }
}
