<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rel_hunter_program`.
 */
class m180707_090511_create_rel_hunter_program_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%rel_hunter_program}}', [
            'hunter_primary_user_id' => $this->integer()->notNull(),
            'program_id' => $this->integer()->notNull()
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%rel_hunter_program}}',
            'Соответствие хантеров и программ'
        );

        //из обоих полей таблицы формируется первичный ключ, т.к. комбинация "hunter_primary_user_id - program_id" всегда уникальна
        $this->addPrimaryKey('pk-rel_hunter_program', '{{%rel_hunter_program}}', ['hunter_primary_user_id', 'program_id']);

        //кроме этого, создаются индексы для каждого поля таблицы
        $this->createIndex(
            'idx-rel_hunter_program-hunter_primary_user_id',
            '{{%rel_hunter_program}}',
            'hunter_primary_user_id'
        );

        $this->createIndex(
            'idx-rel_hunter_program-program_id',
            '{{%rel_hunter_program}}',
            'program_id'
        );

        $this->addForeignKey(
            'fk-rel_hunter_program-user',
            '{{%rel_hunter_program}}',
            'hunter_primary_user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-rel_hunter_program-bounty_program',
            '{{%rel_hunter_program}}',
            'program_id',
            '{{%bounty_program}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rel_hunter_program}}');
    }
}
