<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rel_program_type_module`.
 */
class m180329_135552_create_rel_program_type_module_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%rel_program_type_module}}', [
            'program_type_id' => $this->integer()->notNull(),
            'module_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%rel_program_type_module}}',
            'Соответствие типа программ и модулей в этой программе'
        );

        //из обоих полей таблицы формируется первичный ключ, т.к. комбинация "program_type_id - module_id" всегда уникальна
        $this->addPrimaryKey('pk-rel_program_type_module', '{{%rel_program_type_module}}', ['program_type_id', 'module_id']);

        //кроме этого, создаются индексы для каждого поля таблицы
        $this->createIndex(
            'idx-rel_program_type_module-program_type_id',
            '{{%rel_program_type_module}}',
            'program_type_id'
        );

        $this->createIndex(
            'idx-rel_program_type_module-module_id',
            '{{%rel_program_type_module}}',
            'module_id'
        );

        $this->addForeignKey(
            'f_k_module_id',
            '{{%rel_program_type_module}}',
            'module_id',
            '{{%dir_module}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'f_k_program_type_id',
            '{{%rel_program_type_module}}',
            'program_type_id',
            '{{%dir_program_type}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->batchInsert('{{%rel_program_type_module}}',
            [
                'program_type_id',
                'module_id',
            ],
            [
                [
                    '1',
                    '1'
                ],
                [
                    '1',
                    '2'
                ],
                [
                    '1',
                    '3'
                ],

                [
                    '1',
                    '4'
                ],

                [
                    '2',
                    '1'
                ],
                [
                    '2',
                    '2'
                ],
                [
                    '2',
                    '3'
                ],
                [
                    '2',
                    '4'
                ],
                [
                    '2',
                    '5'
                ],
                [
                    '2',
                    '6'
                ],
                [
                    '2',
                    '7'
                ],
                [
                    '2',
                    '8'
                ],

                [
                    '3',
                    '1'
                ],
                [
                    '3',
                    '2'
                ],
                [
                    '3',
                    '3'
                ],
                [
                    '3',
                    '4'
                ],
                [
                    '3',
                    '5'
                ],
                [
                    '3',
                    '6'
                ],
                [
                    '3',
                    '7'
                ],
                [
                    '3',
                    '8'
                ],
                [
                    '3',
                    '9'
                ],
                [
                    '3',
                    '10'
                ],
                [
                    '3',
                    '11'
                ],
                [
                    '3',
                    '12'
                ],
                [
                    '3',
                    '13'
                ],
                [
                    '3',
                    '14'
                ],
                [
                    '3',
                    '15'
                ],
                [
                    '3',
                    '16'
                ],
                [
                    '3',
                    '17'
                ],
            ]
        );
    }



    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rel_program_type_module}}');
    }
}
