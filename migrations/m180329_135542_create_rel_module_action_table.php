<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rel_module_action`.
 */
class m180329_135542_create_rel_module_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%rel_module_action}}', [
            'module_id' => $this->integer()->notNull(),
            'action_id' => $this->integer()->notNull()
        ], $tableOptions);


        $this->addCommentOnTable(
            '{{%rel_module_action}}',
            'Соответствие конкретных модулей и их действий: Facebook -> like, comment; Twitter -> repost, like и т.д.'
        );

        //из обоих полей таблицы формируется первичный ключ, т.к. комбинация "module_id - action_id" всегда уникальна
        $this->addPrimaryKey('pk-rel_module_action', '{{%rel_module_action}}', ['module_id', 'action_id']);

        //кроме этого, создаются индексы для каждого поля таблицы
        $this->createIndex(
            'idx-rel_module_action-module_id',
            '{{%rel_module_action}}',
            'module_id'
        );

        $this->createIndex(
            'idx-rel_module_action-action_id',
            '{{%rel_module_action}}',
            'action_id'
        );

        $this->addForeignKey(
            'f_module_id',
            '{{%rel_module_action}}',
            'module_id',
            '{{%dir_module}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'f_action_id',
            '{{%rel_module_action}}',
            'action_id',
            '{{%dir_action}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->batchInsert('{{%rel_module_action}}',
            [
                'module_id',
                'action_id',
            ],
            [
                [
                    '1',
                    '1'
                ],
                [
                    '1',
                    '2'
                ],
                [
                    '1',
                    '3'
                ],

                [
                    '2',
                    '1'
                ],
                [
                    '2',
                    '2'
                ],
                [
                    '2',
                    '3'
                ],

                [
                    '3',
                    '1'
                ],
                [
                    '3',
                    '2'
                ],
                [
                    '3',
                    '3'
                ],

                [
                    '4',
                    '1'
                ],

                [
                    '5',
                    '1'
                ],
                [
                    '5',
                    '2'
                ],
                [
                    '5',
                    '3'
                ],

                [
                    '6',
                    '1'
                ],
                [
                    '6',
                    '2'
                ],
                [
                    '6',
                    '3'
                ],

                [
                    '7',
                    '1'
                ],
                [
                    '7',
                    '2'
                ],
                [
                    '7',
                    '3'
                ],

                [
                    '8',
                    '1'
                ],
                [
                    '8',
                    '2'
                ],
                [
                    '8',
                    '3'
                ],

                [
                    '9',
                    '1'
                ],
                [
                    '9',
                    '2'
                ],
                [
                    '9',
                    '3'
                ],

                [
                    '10',
                    '1'
                ],
                [
                    '10',
                    '2'
                ],
                [
                    '10',
                    '3'
                ],

                [
                    '11',
                    '1'
                ],
                [
                    '11',
                    '2'
                ],
                [
                    '11',
                    '3'
                ],

                [
                    '12',
                    '1'
                ],
                [
                    '12',
                    '2'
                ],
                [
                    '12',
                    '3'
                ],

                [
                    '13',
                    '1'
                ],
                [
                    '13',
                    '2'
                ],
                [
                    '13',
                    '3'
                ],

                [
                    '14',
                    '1'
                ],
                [
                    '14',
                    '2'
                ],
                [
                    '14',
                    '3'
                ],

                [
                    '15',
                    '1'
                ],
                [
                    '15',
                    '2'
                ],
                [
                    '15',
                    '3'
                ],

                [
                    '16',
                    '1'
                ],
                [
                    '16',
                    '2'
                ],
                [
                    '16',
                    '3'
                ],

                [
                    '17',
                    '1'
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rel_module_action}}');
    }
}
