<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dir_program_status`.
 */
class m180329_122804_create_dir_program_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dir_program_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%dir_program_status}}',
            'Справочник статусов программ: в ожидании, активна, выполнена, приостановлена и т.д.'
        );

        $this->batchInsert('{{%dir_program_status}}',
            [
                'id',
                'name'
            ],
            [
                [
                    '1',
                    'Active'
                ],
                [
                    '2',
                    'Pending'
                ],
                [
                    '3',
                    'Complete'
                ],
                [
                    '4',
                    'Suspended'
                ],
                [
                    '5',
                    'Not active'
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dir_program_status}}');
    }
}
