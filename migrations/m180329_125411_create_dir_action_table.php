<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dir_action`.
 */
class m180329_125411_create_dir_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dir_action}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%dir_action}}',
            'Справочник действий модуля: Comment, Like, Repost и т.д.'
        );

        $this->batchInsert('{{%dir_action}}',
            [
                'id',
                'name',
                'description'
            ],
            [
                [
                    '1',
                    'Comment',
                    ''
                ],
                [
                    '2',
                    'Like',
                    ''
                ],
                [
                    '3',
                    'Repost',
                    ''
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dir_action}}');
    }
}
