<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dir_module`.
 */
class m180329_131726_create_dir_module_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dir_module}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'status' => "ENUM('1','0') NOT NULL DEFAULT '1'",
            'module_type_id' => $this->integer()->notNull()
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%dir_module}}',
            'Справочник модулей: Twitter, Facebook, блог, форум и т.д.'
        );

        $this->addForeignKey(
            'f_module_type',
            '{{%dir_module}}',
            'module_type_id',
            '{{%dir_module_type}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->batchInsert('{{%dir_module}}',
            [
                'id',
                'name',
                'description',
                'status',
                'module_type_id'
            ],
            [
                [
                    '1',
                    'Facebook',
                    '',
                    '1',
                    '1'
                ],
                [
                    '2',
                    'Twitter',
                    '',
                    '1',
                    '1'
                ],
                [
                    '3',
                    'Google plus',
                    '',
                    '1',
                    '1'
                ],
                [
                    '4',
                    'BitcoinTalk',
                    '',
                    '1',
                    '2'
                ],
                [
                    '5',
                    'Youtube',
                    '',
                    '1',
                    '1'
                ],
                [
                    '6',
                    'Reddit',
                    '',
                    '1',
                    '1'
                ],
                [
                    '7',
                    'Vkontakte',
                    '',
                    '1',
                    '1'
                ],
                [
                    '8',
                    'LinkedIn',
                    '',
                    '1',
                    '1'
                ],
                [
                    '9',
                    'Flickr',
                    '',
                    '1',
                    '1'
                ],
                [
                    '10',
                    'Instagramm',
                    '',
                    '1',
                    '1'
                ],
                [
                    '11',
                    'Pinterest',
                    '',
                    '1',
                    '1'
                ],
                [
                    '12',
                    'Tumblr',
                    '',
                    '1',
                    '1'
                ],
                [
                    '13',
                    'OK',
                    '',
                    '1',
                    '1'
                ],
                [
                    '14',
                    'Renren',
                    '',
                    '1',
                    '1'
                ],
                [
                    '15',
                    'QQ',
                    '',
                    '1',
                    '1'
                ],
                [
                    '16',
                    'Sina Weibo',
                    '',
                    '1',
                    '1'
                ],
                [
                    '17',
                    'Steam',
                    '',
                    '1',
                    '2'
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dir_module}}');
    }
}
