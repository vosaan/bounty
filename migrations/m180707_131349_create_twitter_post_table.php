<?php

use yii\db\Migration;

/**
 * Handles the creation of table `twitter_post`.
 */
class m180707_131349_create_twitter_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%twitter_post}}', [
            'id' => $this->primaryKey(),
            'owner_id' => $this->integer()->notNull(),
            'program_id' => $this->integer()->notNull(),
            'program_module_id' => $this->integer()->notNull(),
            'post_id' => $this->bigInteger(),
            'post_create_date' => $this->integer()->notNull(),
            'post_add_date' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-twitter_post-user',
            '{{%twitter_post}}',
            'owner_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-twitter_post-bounty_program',
            '{{%twitter_post}}',
            'program_id',
            '{{%bounty_program}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-twitter_post-bounty_program_module',
            '{{%twitter_post}}',
            'program_module_id',
            '{{%bounty_program_module}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%twitter_post}}');
    }
}
