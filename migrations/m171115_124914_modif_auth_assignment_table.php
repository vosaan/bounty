<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m180706_074613_modif_auth_assignment_table
 */
class m171115_124914_modif_auth_assignment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%auth_assignment}}', 'user_id', Schema::TYPE_INTEGER);

        $this->addForeignKey(
            'fk-auth_assignment-user',
            '{{%auth_assignment}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_auth_assignment_user_id', '{{%auth_assignment}}');
        $this->alterColumn('{{%auth_assignment}}', 'user_id', Schema::TYPE_STRING . '(64)');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180706_074613_modif_auth_assignment_table cannot be reverted.\n";

        return false;
    }
    */
}
