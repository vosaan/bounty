<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bounty_program_action_reward`.
 */
class m180605_072644_create_bounty_program_action_reward_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bounty_program_action_reward}}', [
            'id' => $this->primaryKey(),
            'program_id' => $this->integer()->notNull(),
            'module_id' => $this->integer()->notNull(),
            'action_id' => $this->integer()->notNull(),
            'reward_level_id' => $this->integer()->notNull(),
            'followers_count' => $this->integer()->notNull(),
            'reward_value' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%bounty_program_action_reward}}',
            'Таблица наград за действия в зависимости от кол-во фолловеров'
        );

        $this->addForeignKey(
            'fk-bounty_program_action_reward-bounty_program',
            '{{%bounty_program_action_reward}}',
            'program_id',
            '{{%bounty_program}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-bounty_program_action_reward-bounty_program_module',
            '{{%bounty_program_action_reward}}',
            'module_id',
            '{{%bounty_program_module}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-bounty_program_action_reward-bounty_program_action',
            '{{%bounty_program_action_reward}}',
            'action_id',
            '{{%bounty_program_action}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk-bounty_program_action_reward-reward_level',
            '{{%bounty_program_action_reward}}',
            'reward_level_id',
            '{{%dir_reward_level}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bounty_program_action_reward}}');
    }
}
