<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dir_module_type`.
 */
class m180329_130741_create_dir_module_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dir_module_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%dir_module_type}}',
            'Справочник типов модулей: соц.сеть, блог, форум и т.д.'
        );

        $this->batchInsert('{{%dir_module_type}}',
            [
                'id',
                'name',
                'description'
            ],
            [
                [
                    '1',
                    'Social network',
                    ''
                ],
                [
                    '2',
                    'Forum',
                    ''
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dir_module_type}}');
    }
}
