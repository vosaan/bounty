<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bounty_program_action`.
 */
class m180330_081255_create_bounty_program_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bounty_program_action}}', [
            'id' => $this->primaryKey(),
            'program_id' => $this->integer()->notNull(),
            'module_id' => $this->integer()->notNull(),
            'action_id' => $this->integer()->notNull(),
            'deadline' => $this->date()->defaultValue(null)
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%bounty_program_action}}',
            'Таблица действий модулей bounty-программ'
        );

        $this->addForeignKey(
            'f_a_program_id',
            '{{%bounty_program_action}}',
            'program_id',
            '{{%bounty_program}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'f_a_module_id',
            '{{%bounty_program_action}}',
            'module_id',
            '{{%bounty_program_module}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'f_a_action_id',
            '{{%bounty_program_action}}',
            'action_id',
            '{{%dir_action}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bounty_program_action}}');
    }
}
