<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bounty_program_module`.
 */
class m180330_081241_create_bounty_program_module_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bounty_program_module}}', [
            'id' => $this->primaryKey(),
            'program_id' => $this->integer()->notNull(),
            'module_id' => $this->integer()->notNull(),
            'module_target_link' => $this->string()->notNull(),
            'module_budget' => $this->float()->notNull(),
            'current_module_status' => $this->integer()->defaultValue(1)->notNull()
        ], $tableOptions);

        $this->addCommentOnTable(
            '{{%bounty_program_module}}',
            'Таблица модулей bounty-программ'
        );

        $this->addForeignKey(
            'f_bounty_program_id',
            '{{%bounty_program_module}}',
            'program_id',
            '{{%bounty_program}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        $this->addForeignKey(
            'f_bounty_module_id',
            '{{%bounty_program_module}}',
            'module_id',
            '{{%dir_module}}',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bounty_program_module}}');
    }
}
