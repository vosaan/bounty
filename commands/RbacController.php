<?php

namespace app\commands;
use yii\console\Controller;
use Yii;

class RbacController extends Controller
{
    public function actionCreateRoles()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->createRole('admin');
        $admin->description = 'Administrator';
        $auth->add($admin);

        $owner = $auth->createRole('owner');
        $owner->description = 'Owner';
        $auth->add($owner);

            /* Owner`s subaccounts */
            $analyst = $auth->createRole('analyst');
            $analyst->description = 'Analyst';
            $auth->add($analyst);

            $manager = $auth->createRole('manager');
            $manager->description = 'Manager';
            $auth->add($manager);

            $support = $auth->createRole('support');
            $support->description = 'Support';
            $auth->add($support);
            /* /Owner`s subaccounts */

        $hunter = $auth->createRole('hunter');
        $hunter->description = 'Hunter';
        $auth->add($hunter);

        echo "Success!";
    }

    public function actionAssignAdminRoleToDefaultUser()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->getRole('admin');
        $auth->assign($admin, 1);

        echo "Success!";
    }
}