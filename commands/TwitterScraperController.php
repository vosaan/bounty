<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 26.06.2018
 * Time: 11:12
 */

namespace app\commands;
use app\helpers\Utils;
use app\models\BountyProgramModule;
use app\models\DirModule;
use app\models\DirProgramStatus;
use yii\console\Controller;
use app\models\BountyProgram;
use yii\helpers\ArrayHelper;
use app\helpers\TwitterAuth;
use app\models\TwitterPost;

class TwitterScraperController extends Controller
{
    public function actionScrape(){
        $modules = $this->getModulesForScrape();

        foreach ($modules as $module) {
            /* Найти ID последнего твита для этого модуля */
            $lastTweet = TwitterPost::find()
                ->where(['program_id' => $module['program_id'], 'program_module_id' => $module['id']])
                ->orderBy(['post_create_date' => SORT_DESC])
                ->limit(1)
                ->asArray()
                ->one();

            $lastTweetId = $lastTweet['post_id'];

            /* Усли такой ID есть, то получить новые твиты для мрдуля начиная с ID и записать их в БД*/
            if ($lastTweetId) {
                $tweets = TwitterAuth::getUserTimeLineFromLastId($module['name_to_scrape'], $lastTweetId);
                $this->addTweets($tweets, $module);
            }
            /* Иначе получить твиты только за сегодня и записать их в БД*/
            else {
                $tweets = TwitterAuth::getUserTimeLineFromToday($module['name_to_scrape']);
                $this->addTweets($tweets, $module);
            }
        }
    }

    public function scrapeForNewProgram(){

    }

    private function addTweets($tweets, $module)
    {
        foreach ($tweets as $tweet) {
            $twitterPost = new TwitterPost();
            $twitterPost->program_id = $module['program_id'];
            $twitterPost->program_module_id = $module['id'];
            $twitterPost->owner_id = $module['owner_id'];
            $twitterPost->post_id = $tweet->id;
            $twitterPost->post_create_date = strtotime($tweet->created_at);
            $twitterPost->post_add_date = time();
            if (!$twitterPost->save()) {
                print_r($twitterPost->errors);
            }
        }
    }

    private function getModulesForScrape()
    {
        /**
         * Получаем ID программ со статусом "Active"
         */
        $activeStatusId = DirProgramStatus::find()->where(['name' => 'Active'])->one()->id;
        $activePrograms = BountyProgram::find()->select(['id'])->where(['status_id' => $activeStatusId])->asArray()->all();

        /**
         * Получаем модули Twitter в активных программах
         */
        $moduleId = DirModule::find()->where(['name' => 'Twitter'])->one()->id;
        $modules = BountyProgramModule::find()
            ->where(
                [
                    'module_id' => $moduleId,
                    'program_id' => ArrayHelper::map($activePrograms, 'id', 'id')
                ]
            )
            ->asArray()
            ->all();

        /**
         * Добавляем к модулям сведения о владельце программы и имя для скрейпинга
         */
        foreach ($modules as &$module) {
            $module['owner_id'] = BountyProgram::find()
                ->where(['id' => $module['program_id']])
                ->one()
                ->owner_id;

            $module['name_to_scrape'] = $this->getScreenNameByModuleTargetLink($module['module_target_link']);
        }

        return $modules;
    }

    private function getScreenNameByModuleTargetLink($targetLink){
        trim($targetLink, '/');
        $pos = strripos($targetLink, '/');
        return substr($targetLink, $pos+1);
    }
}