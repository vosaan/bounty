$(document).ready(function () {
    $('#user-change-name-checker').change(function () {
        if ($(this).is(':checked')) {
            $('#user-change-name-field').prop('disabled', false)
        } else {
            $('#user-change-name-field').prop('disabled', true)
        }
    });

    $('#user-change-email-checker').change(function () {
        if ($(this).is(':checked')) {
            $('#user-change-email-field').prop('disabled', false)
        } else {
            $('#user-change-email-field').prop('disabled', true)
        }
    });

    $('#user-change-password-checker').change(function () {
        if ($(this).is(':checked')) {
            $('#user-change-password-field-1, #user-change-password-field-2').prop('disabled', false)
        } else {
            $('#user-change-password-field-1, #user-change-password-field-2').prop('disabled', true)
        }
    });
});
