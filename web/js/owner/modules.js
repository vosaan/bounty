$(document).ready(function () {
    /**
     * Обрабатывает события нажатий на чекбоксы модулей и действий (рарзоваричает/сворачивает, энаблит/дисаблит поля)
     */
    $('.module-switcher, .action-switcher').each(function () {
        var parent = $(this).parent();
        var moduleData = parent.next();
        enableDisableModuleFields(moduleData, this);
    });

    $('.module-switcher, .action-switcher').change(function () {
        var parent = $(this).parent();
        var moduleData = parent.next();
        enableDisableModuleFields(moduleData, this);
    });


    /**
     * Метод валидации количества остатка токенов. Не допускает отправку формы, если токенов отрицатеьное количество
     */
    $.validator.addMethod('TokensCount', function(value) {
        return parseInt(value) > 0;
    }, 'Balance can not be negative value');
    /******************************************************************************************************************/


    /**
     * Валидирует форму
     * */
    $('#modules-form').validate({
        ignore: ":hidden:not(.tokens-counter)",
        rules: {
            tokenscounter: {
                TokensCount: true
            }
        },
    });
    /******************************************************************************************************************/


    /**
     * Проверяет, выбран ли хотя бы один экшн для модуля
     * */
    $('#save-modules').on('click', function () {
        // $('form').submit(function (e) {
        //     e.preventDefault(e);
        // });

        var checkedModules = $(".module-switcher:checked");
        checkedModules.each(function () {
            var parent = $(this).parent();
            var moduleData = parent.next();
            var actions = moduleData.find('input.action-switcher')

            console.log(actions);

            actions.rules('add', {
                required: true,
                minlength: 1,
                messages: {
                    required: "You must choose at least one action for module!",
                }
            });
        });
    });
    /******************************************************************************************************************/

    /**
     * Слайдер количества фолловеров
     */
    $('[id^="followers-slider"]').slider({
        formatter: function(value) {
            return 'Current value: ' + value;
        }
    });
    /******************************************************************************************************************/

    /**
     * Запрещает отменять и редактировать уже заполненные модули и действия, если программа уже активна
     */
    var checkedFields = $('.module-switcher:checked, .action-switcher:checked');
    var targetLinkFields = $("input.module-target-link:text:not(:disabled)");
    var followersSliders =$("input.followers-count-slider:text:not(:disabled)");
    var rewardValues =$("input.reward-value:text:not(:disabled)");
    var deadlines =$("input[data-function='deadline-date-picker']:text:not(:disabled)");

    if (forbiddenToEdit()){
        checkedFields.each(function () {
            $(this).click(function () {
                return false;
            });
            $(this).parent().addClass('read-only');
        });

        targetLinkFields.each(function () {
            $(this).prop('readonly', true);
        });

        rewardValues.each(function () {
            $(this).prop('readonly', true);
        });

        followersSliders.each(function () {
            $(this).slider('disable');
        });

        deadlines.each(function () {
            $(this).prop('disabled', true);
        });
    }

    function forbiddenToEdit(){
        var controllerAction = $('#modules-form').data('controller-action');
        var programStatus = $('#modules-form').data('program-status');
        if (controllerAction == 'update' && programStatus == 'active'){
            return true;
        } else {
            return false;
        }
    }
    /******************************************************************************************************************/

    /**
     * Считалка остатка токенов
     */

    $('.module-budget').on('input', function () {
        reCountBalance()
    });

    $('.module-switcher').on('change', function () {
        reCountBalance()
    });

    function reCountBalance() {
        var allBudgetFields = $('.module-budget:enabled');
        var allModulesBudget = Number(0);
        allBudgetFields.each(function () {
            allModulesBudget += Number($(this).val());
        });

        var budgetBalance = $('.common-program-budget').data('common-budget') - allModulesBudget;
        $('.common-program-budget').text(budgetBalance);
        $('.tokens-counter').val(budgetBalance);
        if (budgetBalance < 0) {
            $('.common-program-budget').addClass('over-budget');
        } else {
            $('.common-program-budget').removeClass('over-budget');
        }
    }
    /******************************************************************************************************************/

    /**
     *  Не допускает ввода символов, кроме цифр
     */
    $(".module-budget").on("keypress keyup blur",function (event) {
        $(this).val($(this).val().replace(/[^0-9]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    /******************************************************************************************************************/

});

/**
 * Делает поля модулей и действий включеными/выключеными
 * @param moduleData
 * @param item
 */

function enableDisableModuleFields(moduleData, item) {
    if(item.checked) {
        moduleData.find(':input').prop('disabled',false);
        moduleData.find(':input').prop('aria-required',true);
        moduleData.show(200);
        moduleData.find('label').removeClass('disabled-label');
    } else {
        moduleData.find(':input').prop('disabled',true);
        moduleData.find(':input').prop('aria-required',false);
        moduleData.hide(200);
        moduleData.find('label').addClass('disabled-label');
    }
}