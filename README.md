BOUNTY

**Start order:**

1.      Do `yii migrate --migrationPath=@yii/rbac/migrations/` (for Linux/Unix `./yii migrate --migrationPath=@yii/rbac/migrations/`)
2.      Do `yii migrate` (for Linux/Unix `./yii migrate`)
3.      Do `yii rbac/create-roles` (for Linux/Unix `./yii rbac/create-roles`)
4.      Do `yii rbac/assign-admin-role-to-default-user` (for Linux/Unix `./yii rbac/assign-admin-role-to-default-user`)
