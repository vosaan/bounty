<?php
namespace app\helpers;
use Yii;
use Facebook\Facebook;

class FacebookAuth
{
    private static function getParam(){
        return [
            'clientId' => Yii::$app->params['facebookAuth']['clientId'],
            'clientSecret' => Yii::$app->params['facebookAuth']['clientSecret'],
            'redirectUri' => (Yii::$app->controller->module->id == "hunter") ?
                Yii::$app->params['facebookAuth']['redirectUriHunter'] :
                Yii::$app->params['facebookAuth']['redirectUri'],
        ];
    }

    public static function getLinkToAuth()
    {
        $fb = new Facebook(
            [
                'app_id' => self::getParam()['clientId'],
                'app_secret' => self::getParam()['clientSecret'],
                'default_graph_version' => 'v2.2'
            ]
        );

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email', 'user_friends']; // Optional permissions
        return $helper->getLoginUrl(self::getParam()['redirectUri'], $permissions);
    }

    public static function getUserData()
    {
        $fb = new Facebook(
            [
                'app_id' => self::getParam()['clientId'],
                'app_secret' => self::getParam()['clientSecret'],
                'default_graph_version' => 'v2.2'
            ]
        );

        $helper = $fb->getRedirectLoginHelper();
        if (Yii::$app->request->get('state')) {
            $helper->getPersistentDataHandler()->set('state', Yii::$app->request->get('state'));
        }


        try {
            $accessToken = $helper->getAccessToken(self::getParam()['redirectUri']);

        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

        $oAuth2Client = $fb->getOAuth2Client();

        $tokenMetadata = $oAuth2Client->debugToken($accessToken);

        $tokenMetadata->validateAppId(self::getParam()['clientId']); // Replace {app-id} with your app id

        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
            }
        }

        try {
            $response = $fb->get('/me?fields=email,name,id,friends', $accessToken->getValue());
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $user = $response->getGraphUser();

        try {
            $response = $fb->get(
                '/'. $user->getId() .'/permissions',
                $accessToken->getValue()
            );
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $permissions = $response->getDecodedBody();

        $declinedPermissions = [];
        foreach ($permissions['data'] as $permission) {
            if ($permission['status'] === 'declined') {
                $declinedPermissions[] = $permission['permission'];
            }
        }

        if (!empty($declinedPermissions)) {
            $url_fb = $helper->getReRequestUrl(self::getParam()['redirectUri'], $declinedPermissions);
            $userData = (object)[
                'warning' => 'not enough fields',
                'relogin_link' => $url_fb,
                'required_fields' => implode(',', $declinedPermissions)
            ];
        } else {
            $userData = (object)[
                'warning' => '',
                'soc_network_name' => 'facebook',
                'soc_user_id' => $user->getId(),
                'soc_user_name' => $user->getName(),
                'soc_user_email' => $user->getEmail(),
                'soc_user_pic' => '',
                'soc_user_followers_count' => $user->getField('friends')->getTotalCount()
            ];
        }

        return $userData;
    }

    public static function getFeed(){
        $fb = new Facebook(
            [
                'app_id' => self::getParam()['clientId'],
                'app_secret' => self::getParam()['clientSecret'],
                'default_graph_version' => 'v2.2'
            ]
        );

        return $fb;
    }
}