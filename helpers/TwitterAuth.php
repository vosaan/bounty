<?php
namespace app\helpers;
use Yii;
use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterAuth
{

    private static function getParam(){
        return [
            'consumerKey' => Yii::$app->params['twitterAuth']['consumerKey'],
            'consumerSecret' => Yii::$app->params['twitterAuth']['consumerSecret'],
            'urlCallback' => (Yii::$app->controller->module->id == "hunter") ?
                Yii::$app->params['twitterAuth']['urlCallbackHunter'] :
                Yii::$app->params['twitterAuth']['urlCallback']
        ];
    }

    public static function getLinkToAuth()
    {
        $twitterOauth = new TwitterOAuth(self::getParam()['consumerKey'], self::getParam()['consumerSecret']);

        $requestToken = $twitterOauth->oauth('oauth/request_token', ['oauth_callback' => self::getParam()['urlCallback']]);

        if($twitterOauth->getLastHttpCode() != 200) {
            throw new \Exception('There was a problem performing this request');
        }

        $session = Yii::$app->session;
        $session->set('oauth_token', $requestToken['oauth_token']);
        $session->set('oauth_token_secret', $requestToken['oauth_token_secret']);

        $tw_auth_url = $twitterOauth->url(
            //'oauth/authorize', [
            'oauth/authenticate', [
                'oauth_token' => $requestToken['oauth_token']
            ]
        );

        return $tw_auth_url;
    }

    public static function getUserData($oauthVerifier)
    {
        $session = Yii::$app->session;
        $signupUrl = $session->get('url');

        if (!$oauthVerifier || !$session->get('oauth_token') || !$session->get('oauth_token_secret')) {
            header('Location: ' . $signupUrl);
        }

        $connection = new TwitterOAuth(
            self::getParam()['consumerKey'],
            self::getParam()['consumerSecret'],
            $session->get('oauth_token'),
            $session->get('oauth_token_secret')
        );

        $token = $connection->oauth('oauth/access_token', ['oauth_verifier' => $oauthVerifier]);

        $connection = new TwitterOAuth(
            self::getParam()['consumerKey'],
            self::getParam()['consumerSecret'],
            $token['oauth_token'],
            $token['oauth_token_secret']
        );

        $response = $connection->get("account/verify_credentials", ['include_email' => 'true']);

        $userData = (object)[
            'soc_network_name' => 'twitter',
            'soc_user_id' => $response->id,
            'soc_user_name' => $response->name,
            'soc_user_email' => $response->email,
            'soc_user_pic' => $response->profile_image_url_https,
            'soc_user_screen_name' => $response->screen_name,
            'soc_user_followers_count' => $response->followers_count,
        ];

        return $userData;
    }

    public static function getUserTimeLineFromLastId($screenName = null, $lastTweetId){
        $connection = self::getConnectionWithToken();
        $data = $connection
            ->get("statuses/user_timeline", [
                'count' => 200,
                'exclude_replies' => true,
                'screen_name' => $screenName,
                'trim_user' => true,
                'since_id' => $lastTweetId
            ]);
        return $data;
    }

    public static function getUserTimeLineFromToday($screenName = null){

    $connection = self::getConnectionWithToken();
    $posts = $connection
        ->get("statuses/user_timeline", [
            'count' => 200,
            'exclude_replies' => true,
            'screen_name' => $screenName,
            'trim_user' => true
        ]);


        $data = [];

        foreach ($posts as $post) {
            if (strtotime($post->created_at) >= strtotime('today midnight')) {
                $data[] = $post;
            }
        }

//        $data[0] = [
//            'created_at' => 'Tue Jul 10 05:23:09 +0000 2018',
//            'id' => 1016553388807524352,
//        ];
//
//        $data[1] = [
//            'created_at' => 'Mon Jul 09 09:56:14 +0000 2018',
//            'id' => 1016259725539749888,
//        ];
//
//        $data[2] = [
//            'created_at' => 'Mon Jul 09 07:23:41 +0000 2018',
//            'id' => 1016221337415831552,
//        ];
//
//        $data[3] = [
//            'created_at' => 'Mon Jul 09 06:09:02 +0000 2018',
//            'id' => 1016202548045201409,
//        ];
//
//        echo "Now: " . time();
//        echo "\n\r";
//        echo strtotime('today midnight');
//        echo "\n\r";
//
//        foreach ($data as $datum) {
//            echo strtotime($datum['created_at']);
//            echo "\n\r";
//        }
//
//        foreach ($data as $datum) {
//            if ((integer)strtotime($datum['created_at']) > (integer)strtotime('today midnight')){
//                echo $datum['created_at'];
//                echo "\n\r";
//            }
//        }






        return $data;
    }

    public static function getReTweetersByTweetId($tweetId){
        $connection = self::getConnectionWithToken();
        return $connection->get("statuses/retweets", ['id' => $tweetId]);
    }

    public static function getLikesByUserId($userId){
        $connection = self::getConnectionWithToken();
        return $connection->get("favorites/list", ['screen_name' => $userId]);
    }

    private static function getConnectionWithToken() {
        $connection = new TwitterOAuth(
            Yii::$app->params['twitterAuth']['consumerKey'],
            Yii::$app->params['twitterAuth']['consumerSecret'],
            Yii::$app->params['twitterAuth']['accessToken'],
            Yii::$app->params['twitterAuth']['accessTokenSecret']
        );

        return $connection;
    }
}