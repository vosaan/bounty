<?php
/**
 * Created by PhpStorm.
 * User: Vo
 * Date: 07.06.2018
 * Time: 20:24
 */

namespace app\helpers;
use app\models\BountyProgramActionReward;
use app\models\DirRewardLevel;
use app\models\RelProgramTypeModule;
use app\models\BountyProgramModule;
use app\models\RelModuleAction;
use app\models\BountyProgramAction;
use app\models\SocAccount;
use yii\helpers\ArrayHelper;

class BountyProgramHlp
{
    public static function getAvailableModules($programTypeId){

        $availableModules = self::getAvailableModulesByProgramType($programTypeId);
        foreach ($availableModules as $module) {
            $actions = [];
            $availableActions = self::getAvailableModuleActionsByModuleId($module->module_id);
            foreach ($availableActions as $action) {
                $action->rewards = self::getAvailableActionRewards();
                $actions[] = $action;
            }
            $module->actions = $actions;
        }
        return $availableModules;
    }



    public static function getCurrentModules($programTypeId, $programId){
        $currentModules = BountyProgramModule::find()->where(['program_id' => $programId])->all();
        $availableModules = self::getAvailableModules($programTypeId);
        if (!$currentModules) {
            return $availableModules;
        } else {
            foreach ($availableModules as &$availableModule) {
                foreach ($currentModules as $currentModule) {

                    $currentModuleActions = BountyProgramAction::find()->where(['module_id' => $currentModule->id])->all();
                    $availableModuleActions = self::getAvailableModuleActionsByModuleId($currentModule->module_id);

                    if (!$currentModuleActions) {
                        $currentModule->actions = $availableModuleActions;
                        foreach ($currentModule->actions as $action) {
                            $action->rewards = self::getAvailableActionRewards();
                        }
                    } else {
                        foreach ($availableModuleActions as &$availableModuleAction) {
                            foreach ($currentModuleActions as $currentModuleAction) {

                                $currentActionRewards = BountyProgramActionReward::find()->where(['action_id' => $currentModuleAction->id])->all();
                                $availableActionRewards = self::getAvailableActionRewards();

                                if (!$currentActionRewards) {
                                    $currentModuleAction->rewards = $availableActionRewards;
                                } else {
                                    foreach ($availableActionRewards as &$availableActionReward) {
                                        foreach ($currentActionRewards as $currentActionReward){
                                            if ($availableActionReward->reward_level_id == $currentActionReward->reward_level_id) {
                                                $availableActionReward = $currentActionReward;
                                            }
                                        }
                                    }
                                    $currentModuleAction->rewards = $availableActionRewards;
                                }

                                if ($availableModuleAction->action_id == $currentModuleAction->action_id) {
                                    $availableModuleAction = $currentModuleAction;
                                }

                            }
                            $availableModuleAction->rewards = self::getAvailableActionRewards();
                        }
                        $currentModule->actions = $availableModuleActions;
                    }

                    if ($availableModule->module_id == $currentModule->module_id) {
                        $availableModule = $currentModule;
                    }
                }
            }
        }

        return $availableModules;
    }

    public static function saveModulesData($formData, $programId){
        foreach ($formData as $moduleData) {
            $module = new BountyProgramModule();
            $module->program_id = $programId;
            $module->module_id = $moduleData['module_id'];
            $module->module_target_link = $moduleData['module_target_link'];
            $module->module_budget = $moduleData['module_budget'];
            if ($module->validate()) {
                $module->save();
            } else {
                Utils::fOut($module->errors);
            }

            if ($moduleActions = $moduleData['actions']) {
                foreach ($moduleActions as $moduleAction) {
                    if ($moduleAction['action_id']) {
                        $action = new BountyProgramAction();
                        $action->program_id = $programId;
                        $action->module_id = $module->id;
                        $action->action_id = $moduleAction['action_id'];
                        $action->deadline = $moduleAction['deadline'];

                        if ($action->validate()) {
                            $action->save();
                        } else {
                            Utils::fOut($action->errors);
                        }

                        if ($actionRewards = $moduleAction['rewards']) {
                            foreach ($actionRewards as $rewardLevelId => $actionReward) {
                                $moduleActionReward = new BountyProgramActionReward($rewardLevelId);
                                $moduleActionReward->program_id = $programId;
                                $moduleActionReward->module_id = $module->id;
                                $moduleActionReward->action_id = $action->id;
                                $moduleActionReward->followers_count = $actionReward['followers_count'];
                                $moduleActionReward->reward_value = $actionReward['reward_value'];

                                if ($moduleActionReward->validate()) {
                                    $moduleActionReward->save();
                                } else {
                                    Utils::fOut($moduleActionReward->errors);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function getAvailableModulesByProgramType($programTypeId){
        /* Получаем ID доступных модулей для текущего типа программы */
        $availableModulesIds = RelProgramTypeModule::find()
            ->select('module_id')
            ->where(['program_type_id' => $programTypeId])
            ->column();

        /* Объекты доступных модулей */
        $availableModules = [];
        foreach ($availableModulesIds as $moduleId) {
            $newModule = new BountyProgramModule($moduleId);
            $availableModules[] = $newModule;
        }

        return $availableModules;
    }

    public static function getAvailableModuleActionsByModuleId($moduleId){
        /* Получаем ID доступных действий для текущего модуля */
        $availableActionsIds = RelModuleAction::find()
            ->select('action_id')
            ->where(['module_id' => $moduleId])
            ->column();

        /* Объекты доступных действий */
        $availableActions = [];
        foreach ($availableActionsIds as $actionId) {
            $newAction = new BountyProgramAction($actionId);
            $availableActions[] = $newAction;
        }

        return $availableActions;
    }

    public static function getAvailableActionRewards(){
        /* Получаем ID доступных наград */
        $availableRewardLevelIds = DirRewardLevel::find()
            ->select('id')
            ->column();

        /* Объекты доступных нагдар */
        $availableActionRewards = [];
        foreach ($availableRewardLevelIds as $rewardLevelId) {
            $newActionReward = new BountyProgramActionReward($rewardLevelId);
            $availableActionRewards[] = $newActionReward;
        }

        return $availableActionRewards;
    }
    public static function getProgramModules($program){
        $programModules = [];
        foreach ($program->bountyProgramModules as $module) {
            $programModules[] = $module->module->name;
        }
        return implode(',', $programModules);
    }

    public static function getStake($primaryUserId, $socNetworkName, $actionId)
    {
        $followersCount = SocAccount::find()
            ->where(['primary_user_id' => $primaryUserId, 'soc_network_name' => strtolower($socNetworkName)])
            ->one()
            ->soc_user_followers_count;

        if ($followersCount) {
            $rewardsList = BountyProgramActionReward::find()
                ->select(['followers_count', 'reward_value'])
                ->where(['action_id' => $actionId])
                ->all();

            $rewardsList = ArrayHelper::map($rewardsList, 'followers_count', 'reward_value');

            $rewardsList[$followersCount] = 'x';
            ksort($rewardsList);

            reset($rewardsList);
            $key = 0;
            foreach ($rewardsList as $k => $v) {
                if ($v !== 'x') {
                    $key = $k;
                } else {
                    return $rewardsList[$key];
                }
            }
        }
    }
}