<?php
namespace app\helpers;
use yii\helpers\Url;
use Yii;

class Mail
{
    public static function sendActivationMail($email, $activationCode)
    {
        $activationLink = Url::home(true) . "activation/" . $activationCode;

        $msg_html  = "<html><body style='font-family:Arial,sans-serif;'>";
        $msg_html .= "<p>Please verify your email address by clicking the following link:</p>\r\n";
        $msg_html .= "<p></p>\r\n";
        $msg_html .= "<p><a href='". $activationLink."'>Activation link</a></p>\r\n";
        $msg_html .= "<p>This link is valid for one time login for 24 hours.</p>\r\n";
        $msg_html .= "</body></html>";

        Yii::$app->mailer->compose()
            ->setFrom('no-reply@bounty.solutions') //не надо указывать если указано в common\config\main-local.php
            ->setTo($email) // кому отправляем - реальный адрес куда придёт письмо формата asdf @asdf.com
            ->setSubject('Verify your email address') // тема письма
            ->setHtmlBody($msg_html) // текст письма с HTML
            ->send();
    }

    public static function sendPasswordMail($email, $password)
    {
        $msg_html  = "<html><body style='font-family:Arial,sans-serif;'>";
        $msg_html .= "<p>Now you can log in with your social network account or with the following credentials:</p>\r\n";
        $msg_html .= "<p></p>\r\n";
        $msg_html .= "<p>e-mail: ". $email ."<br>\r\n";
        $msg_html .= "Password: ". $password ."</p>\r\n";
        $msg_html .= "</body></html>";

        Yii::$app->mailer->compose()
            ->setFrom('no-reply@bounty.solutions') //не надо указывать если указано в common\config\main-local.php
            ->setTo($email) // кому отправляем - реальный адрес куда придёт письмо формата asdf @asdf.com
            ->setSubject('Verify your email address') // тема письма
            ->setHtmlBody($msg_html) // текст письма с HTML
            ->send();
    }

    public static function sendLinkingRequest($email, $requestCode, $primaryUserId)
    {
        $secret = Yii::$app->encrypter->encrypt($requestCode . '/' . $primaryUserId);

        $requestLink = Url::home(true)
            . Yii::$app->controller->module->id
            . '/'
            . Yii::$app->controller->id
            . '/linking-request/' . $secret;

        $msg_html  = "<html><body style='font-family:Arial,sans-serif;'>";
        $msg_html .= "<p>Please confirm:</p>\r\n";
        $msg_html .= "<p></p>\r\n";
        $msg_html .= "<p><a href='".$requestLink."'>Confirm</a></p>\r\n";
        $msg_html .= "</body></html>";

        Yii::$app->mailer->compose()
            ->setFrom('no-reply@bounty.solutions') //не надо указывать если указано в common\config\main-local.php
            ->setTo($email) // кому отправляем - реальный адрес куда придёт письмо формата asdf @asdf.com
            ->setSubject('Verify your email address') // тема письма
            ->setHtmlBody($msg_html) // текст письма с HTML
            ->send();
    }

    public static function sendChangeMail($email, $oldEmail)
    {
        $secret = Yii::$app->encrypter->encrypt($email . '/' . $oldEmail);
        $requestLink = Url::home(true)
            . Yii::$app->controller->module->id
            . '/'
            . Yii::$app->controller->id
            . '/email-changing/' . $secret;

        $msg_html  = "<html><body style='font-family:Arial,sans-serif;'>";
        $msg_html .= "<p>Please confirm e-mail changing:</p>\r\n";
        $msg_html .= "<p></p>\r\n";
        $msg_html .= "<p><a href='".$requestLink."'>Confirm</a></p>\r\n";
        $msg_html .= "</body></html>";

        Yii::$app->mailer->compose()
            ->setFrom('no-reply@bounty.solutions') //не надо указывать если указано в common\config\main-local.php
            ->setTo($email) // кому отправляем - реальный адрес куда придёт письмо формата asdf @asdf.com
            ->setSubject('Verify your email address') // тема письма
            ->setHtmlBody($msg_html) // текст письма с HTML
            ->send();
    }

    public static function sendChangePassword($email, $password)
    {
        $msg_html  = "<html><body style='font-family:Arial,sans-serif;'>";
        $msg_html .= "<p>Your new password: " . $password . "</p>\r\n";
        $msg_html .= "</body></html>";

        Yii::$app->mailer->compose()
            ->setFrom('no-reply@bounty.solutions') //не надо указывать если указано в common\config\main-local.php
            ->setTo($email) // кому отправляем - реальный адрес куда придёт письмо формата asdf @asdf.com
            ->setSubject('Verify your email address') // тема письма
            ->setHtmlBody($msg_html) // текст письма с HTML
            ->send();
    }
}