<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06.07.2018
 * Time: 15:18
 */

namespace app\helpers;
use yii\base\DynamicModel;

class DynamicModels
{
    public static function getDynamicModelUserChangeName()
    {
        $userChangeNameModel = new DynamicModel(['name']);
        $userChangeNameModel
            ->addRule(['name'], 'required')
            ->addRule('name', 'string', ['min' => 3, 'max' => 24]);

        return $userChangeNameModel;
    }

    public static function getDynamicModelUserChangeEmail()
    {
        $userChangeEmailModel = new DynamicModel(['email']);
        $userChangeEmailModel
            ->addRule('email', 'trim')
            ->addRule('email', 'email')
            ->addRule('email', 'required')
            ->addRule('email', 'string', ['min' => 3, 'max' => 24])
            ->addRule('email', 'unique', ['targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'])
            ->addRule('email', 'unique', ['targetClass' => '\app\models\SocAccount', 'targetAttribute' => 'soc_user_email', 'message' => 'This email address has already been taken.']);

        return $userChangeEmailModel;
    }

    public static function getDynamicModelUserChangePassword()
    {
        $userChangePasswordModel = new DynamicModel(['password', 'confirmPassword']);
        $userChangePasswordModel
            ->addRule(['password', 'confirmPassword'], 'string', ['min' => 6])
            ->addRule(['password', 'confirmPassword'], 'required')
            ->addRule(['password', 'confirmPassword'], 'compare', ['compareAttribute' => 'password', 'operator'=>'==', 'message' => 'Passwords don`t match']);

        return $userChangePasswordModel;
    }
}