<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.01.2018
 * Time: 16:36
 */

namespace app\helpers;
use Yii;

class GoogleAuth
{
    private static function getParam(){
        return [
            'clientId' => Yii::$app->params['googleAuth']['clientId'],
            'clientSecret' => Yii::$app->params['googleAuth']['clientSecret'],
            'redirectUri' => (Yii::$app->controller->module->id == "hunter") ?
                Yii::$app->params['googleAuth']['redirectUriHunter'] :
                Yii::$app->params['googleAuth']['redirectUri'],
            'projectId' => Yii::$app->params['googleAuth']['projectId'],
            'authUri' => Yii::$app->params['googleAuth']['authUri'],
            'tokenUri' => Yii::$app->params['googleAuth']['tokenUri'],
            'authProvider_x509_cert_url' => Yii::$app->params['googleAuth']['authProvider_x509_cert_url'],
            'scope' => Yii::$app->params['googleAuth']['scope'],
            'userInfoUrl' => Yii::$app->params['googleAuth']['userInfoUrl'],
            'apiKey' => Yii::$app->params['googleAuth']['apiKey']
        ];
    }    
	
	public static function getLinkToAuth()
    {
        $params = array(
            'redirect_uri'  => self::getParam()['redirectUri'],
            'response_type' => 'code',
            'client_id'     => self::getParam()['clientId'],
            'scope'         => self::getParam()['scope'],
        );

        return self::getParam()['authUri'] . '?' . urldecode(http_build_query($params));
    }

    public static function getUserData($code)
    {
        $params = [
            'client_id'     => self::getParam()['clientId'],
            'client_secret' => self::getParam()['clientSecret'],
            'redirect_uri'  => self::getParam()['redirectUri'],
            'grant_type'    => 'authorization_code',
            'code'          => $code
        ];

        $url = self::getParam()['tokenUri'];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);

        $tokenInfo = json_decode($result, true);

        $token = $tokenInfo['access_token'];

        $params['access_token'] = $token;

        $response = json_decode(file_get_contents(self::getParam()['userInfoUrl'] . '?' . urldecode(http_build_query($params))), true);

        $userData = (object)[
            'soc_network_name' => 'google',
            'soc_user_id' => $response['id'],
            'soc_user_name' => $response['name'],
            'soc_user_email' => $response['email'],
            'soc_user_pic' => $response['picture'],
            'soc_user_followers_count' => self::getFollowersCount($response['id'], self::getParam()['apiKey']),
        ];

        return $userData;
    }

    private function getFollowersCount($userId, $apiKey){
        $google = file_get_contents( 'https://www.googleapis.com/plus/v1/people/' . $userId . '?key=' . $apiKey );
        return json_decode( $google )->circledByCount;
    }
}