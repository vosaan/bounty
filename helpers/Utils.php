<?php
namespace app\helpers;
use Yii;

class Utils
{
    public static function fOut($data, $die=0)
    {
        echo "<pre>" . print_r($data, 1) . "</pre>";
        if ($die) die;
    }

    public static function getSignupUserRoleByUrl($url)
    {
        $url = trim($url, '/');
        return ($strpos = mb_strpos($url, '/')) !== false ? mb_substr($url, 0, $strpos, 'utf8') : false;
    }

    public static function assignRole($role, $userId)
    {
        $auth = Yii::$app->authManager;
        $roleObj = $auth->getRole($role);
        $auth->assign($roleObj, $userId);
    }

    public static function prepareUrl($url)
    {
        $url = urlencode($url);
        $url = str_replace("+", "%2B", $url);
        $url = urldecode($url);
        $url = trim($url, '3D');
        return $url;
    }

//    public static function getCurrentUserData(){
//        $user = Yii::$app->user->identity;
//        if ($user) {
//
//            $currentUserData = [];
//
//            if ($user['username'] !== 'soc_user') {
//                $currentUserData['username'] = $user['username'];
//            } elseif ($user['twitter_name']) {
//                $currentUserData['username'] = $user['twitter_name'];
//            } elseif ($user['facebook_name']) {
//                $currentUserData['username'] = $user['facebook_name'];
//            } elseif ($user['google_name']) {
//                $currentUserData['username'] = $user['google_name'];
//            }
//
//            $currentUserData['role'] = $user->role['item_name'];
//
//            return $currentUserData;
//        }
//    }

//    public static function moduleInProgram($modules, $moduleId)
//    {
//        return array_key_exists($moduleId, $modules);
//    }
//
//    public static function getModuleBudget($modules, $moduleId)
//    {
//        $module = $modules[$moduleId];
//        return (!empty($module['budget'])) ? (string)$module['budget'] : null;
//    }
//
//    public static function getModuleTargetLink($modules, $moduleId)
//    {
//        $module = $modules[$moduleId];
//        return (!empty($module['link'])) ? (string)$module['link'] : null;
//    }
//
//    public static function getModuleStatus($modules, $moduleId)
//    {
//        $module = $modules[$moduleId];
//        return (!empty($module['current_module_status'])) ? (string)$module['current_module_status'] : null;
//    }
//
//    public static function actionsInModule($modules, $moduleId, $actionId)
//    {
//        $actions = $modules[$moduleId]['actions'];
//
//        return in_array($actionId, (array)$actions) ? true : false;
//    }


}