<?php
namespace app\helpers;
use app\models\SocAccount;
use yii\helpers\Html;
use Yii;

class SocUser
{
    public static function create($userData)
    {
        $account = new SocAccount();
        $account->soc_network_name = $userData->soc_network_name;
        $account->soc_user_id = (string)$userData->soc_user_id;
        $account->soc_user_name = $userData->soc_user_name;
        $account->soc_user_email = $userData->soc_user_email;
        $account->soc_user_pic = $userData->soc_user_pic;
        $account->primary_user_id = $userData->primary_user_id;
        $account->soc_user_screen_name = $userData->soc_user_screen_name;
        $account->created_at = time();
        $account->soc_user_followers_count = $userData->soc_user_followers_count;
        $account->linking_request_code = Yii::$app->security->generateRandomString(24);
        $account->save();
    }

    public static function getFilledNetworks($linkedAccounts)
    {
        $networks = [
            'twitter' => null,
            'facebook' => null,
            'google' => null
        ];

        foreach ($linkedAccounts as $account) {
            if ($account->soc_network_name == 'twitter') {
                if (!isset($networks['twitter'])) {
                    $networks['twitter'] = true;
                }
            }

            if ($account->soc_network_name == 'facebook') {
                if (!isset($networks['facebook'])) {
                    $networks['facebook'] = true;
                }
            }

            if ($account->soc_network_name == 'google') {
                if (!isset($networks['google'])) {
                    $networks['google'] = true;
                }
            }
        }

        return $networks;

    }

    public static function getNetworksTable($linkedAccounts)
    {
        $networks = self::getFilledNetworks($linkedAccounts);

        $table = '';

        if (isset($networks['google'])) {
            $table  .= '<tr><td>Google+</td><td class="text-right">Done</td></tr>';
        } else {
            $table  .= '<tr><td>Google+</td><td class="text-right">'
                . Html::a('Connect', GoogleAuth::getLinkToAuth())
                . '</td></tr>';
        }

        if (isset($networks['facebook'])) {
            $table  .= '<tr><td>Facebook</td><td class="text-right">Done</td></tr>';
        } else {
            $table  .= '<tr><td>Facebook</td><td class="text-right">'
                . Html::a('Connect', FacebookAuth::getLinkToAuth())
                . '</td></tr>';
        }

        if (isset($networks['twitter'])) {
            $table  .= '<tr><td>Twitter</td><td class="text-right">Done</td></tr>';
        } else {
            $table  .= '<tr><td>Twitter</td><td class="text-right">'
                    . Html::a('Connect', TwitterAuth::getLinkToAuth())
                    . '</td></tr>';
        }

        return $table;
    }
}