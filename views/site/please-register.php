<?php
use yii\helpers\Html;
?>
<h3 class="text-center">Unfortunately we can't identify you either as owner, or as hunter.
    Apparently you are not registered in our system.
    Please sign up either as <?=Html::a('owner', '/owner/signup')?> or as <?=Html::a('hunter', '/hunter/signup')?>.
</h3>
