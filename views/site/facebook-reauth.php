<?php
use yii\helpers\Html;
?>
<h3 class="text-center">To proceed you need to give access to the following profile data: <strong><?=$userData->required_fields?></strong>.</h3>
<h3 class="text-center">Please, <?=Html::a('follow this link to reAuth', $userData->relogin_link)?>.</h3>
</h3>