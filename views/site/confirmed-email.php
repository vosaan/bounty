<div class="container text-center">
    <h1>Now your e-mail is confirmed!</h1>
    <p>Now you can <a href="/login">log in</a> with your social network account or with credentials that were sent to your email.</p>
</div>