<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\helpers\TwitterAuth;
use app\helpers\FacebookAuth;
use app\helpers\GoogleAuth;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-4 col-lg-offset-4">
        <div class="site-login">
            <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

            <p class="text-center">Please fill out the following fields to login:</p>

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
            ]); ?>

            <?= $form->field($model, 'email')->textInput() ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?php if (Yii::$app->session->hasFlash('not-activated')): ?>
                <div class="alert-danger"><?= Yii::$app->session->getFlash('not-activated') ?></div>
            <?php endif; ?>

            <?= $form->field($model, 'rememberMe')->checkbox([
                //'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
            ]) ?>

            <div class="text-center">
                If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
            </div>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary center-block', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

            <hr>
            <h3 class="text-center">Login with social networks</h3>
            <div class="row">
                <div class="col-lg-4 text-right">
                    <?= Html::a(Html::img('@web/images/twitter.png', ['alt' => 'Auth with Twitter']), TwitterAuth::getLinkToAuth()) ?>
                </div>
                <div class="col-lg-4 text-center">
                    <?= Html::a(Html::img('@web/images/facebook.png', ['alt' => 'Auth with Facebook']), FacebookAuth::getLinkToAuth()) ?>
                </div>
                <div class="col-lg-4 text-left">
                    <?= Html::a(Html::img('@web/images/g+.png', ['alt' => 'Auth with Google+']), GoogleAuth::getLinkToAuth()) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!---->
<!---->
<!---->
<!---->
<!--<div class="site-login">-->
<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->
<!---->
<!--    <p>Please fill out the following fields to login:</p>-->
<!---->
<!--    --><?php //$form = ActiveForm::begin([
//        'id' => 'login-form',
//        'layout' => 'horizontal',
//        'fieldConfig' => [
//            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
//            'labelOptions' => ['class' => 'col-lg-1 control-label'],
//        ],
//    ]); ?>
<!---->
<!--        --><?//= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
<!---->
<!--        --><?//= $form->field($model, 'password')->passwordInput() ?>
<!---->
<!--        --><?php //if (Yii::$app->session->hasFlash('not-activated')): ?>
<!--            <div class="alert-danger">--><?//= Yii::$app->session->getFlash('not-activated') ?><!--</div>-->
<!--        --><?php //endif; ?>
<!---->
<!--        --><?//= $form->field($model, 'rememberMe')->checkbox([
//            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
//        ]) ?>
<!---->
<!--        <div style="color:#999;margin:1em 0">-->
<!--            If you forgot your password you can --><?//= Html::a('reset it', ['site/request-password-reset']) ?><!--.-->
<!--        </div>-->
<!---->
<!--        <div class="form-group">-->
<!--            <div class="col-lg-offset-1 col-lg-11">-->
<!--                --><?//= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
<!--            </div>-->
<!--        </div>-->
<!---->
<!--    --><?php //ActiveForm::end(); ?>
<!---->
<!--    --><?//= Html::a(Html::img('@web/images/twitter.png', ['alt' => 'Auth with twitter']), TwitterAuth::getLinkToAuth()) ?>
<!--    --><?//= Html::a(Html::img('@web/images/facebook.png', ['alt' => 'Auth with Facebook']), FacebookAuth::getLinkToAuth()) ?>
<!--</div>-->
