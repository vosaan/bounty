<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\helpers\TwitterAuth;
use app\helpers\FacebookAuth;
use app\helpers\GoogleAuth;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-4 col-lg-offset-4">
        <div class="site-signup">
            <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

            <p class="text-center">Please fill out the following fields to signup:</p>

            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'username')->textInput() ?>

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'reCaptcha')->widget(
                \himiklab\yii2\recaptcha\ReCaptcha::className(),
                ['siteKey' => '6LcdL0UUAAAAAPTjrOzo_rXNds2tz4E0KKBWf1Bt']
            )->label('') ?>

            <div class="form-group">
                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary center-block', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <?php if ($role == 'hunter') : ?>
                <hr>
                <h3 class="text-center">Sign up with social networks</h3>
                <div class="row">
                    <div class="col-lg-4 text-right">
                        <?= Html::a(Html::img('@web/images/twitter.png', ['alt' => 'Auth with Twitter']), TwitterAuth::getLinkToAuth()) ?>
                    </div>
                    <div class="col-lg-4 text-center">
                        <?= Html::a(Html::img('@web/images/facebook.png', ['alt' => 'Auth with Facebook']), FacebookAuth::getLinkToAuth()) ?>
                    </div>
                    <div class="col-lg-4 text-left">
                        <?= Html::a(Html::img('@web/images/g+.png', ['alt' => 'Auth with Google+']), GoogleAuth::getLinkToAuth()) ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
