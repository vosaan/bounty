<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="site-request-password-reset">
        <section>
            <h1><?= Html::encode($this->title) ?></h1>

            <p class="common-text text-center">Please fill out your email. A link to reset password will be sent there.</p>

            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                        <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'class' => 'common-form-input', 'placeholder' => 'Email'])->label('') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Send', ['class' => 'common-form-submit']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </section>
    </div>
</div>
