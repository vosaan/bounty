<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="site-reset-password">
        <section>
            <h1>RESET PASSWORD</h1>

            <p class="common-text text-center">Please choose your new password:</p>

            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'class' => 'common-form-input', 'placeholder' => 'Password'])->label('') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'common-form-submit']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </section>
    </div>
</div>
